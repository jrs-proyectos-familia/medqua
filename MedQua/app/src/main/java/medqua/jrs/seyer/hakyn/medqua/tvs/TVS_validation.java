package medqua.jrs.seyer.hakyn.medqua.tvs;

import android.content.Context;
import android.widget.TextView;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_rules;
import medqua.jrs.seyer.hakyn.medqua.tools.TOOL_validation;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 2/03/19 a las 11:56 PM
 * @license Joaquin Reyes Sanchez
 */

public abstract class TVS_validation {

    private boolean success = true;

    /**
     *
     * @param tvErrors Indica el arreglo de los componentes tipo error para mostrar el error de validacion
     * @param rules Indica el arreglo de las reglas para validar dicho componente
     * @param ctx Indica el contexto de la Activity
     * @return boolean True -> pasó la validación; False -> No pasó la validación
     */
    public boolean validate (Object [] tvErrors, FRAME_rules[] rules, Context ctx) {
        this.success = true;

        int index = 0;

        TOOL_validation val = new TOOL_validation(ctx);

        for (FRAME_rules rule: rules) {
            // Aqui van a ir las validaciones obligatorias por default
            if (rule.isValEmpty() && rule.getValMin() > 0 && rule.getValMax() > 0) {
                String error = val.valEmptyMinMax(this.getData(rule.getNameRule()), rule.getValMin(), rule.getValMax());
                if (!error.equals("")) {
                    this.launchError((TextView)tvErrors[index], error);
//                    break;
                }
            } else if (rule.getValMin() > 0 && rule.getValMax() > 0 && !rule.isValEmpty()) {
                String error = val.valOptionalEmptyMinMax(this.getData(rule.getNameRule()), rule.getValMin(), rule.getValMax());

                if (!error.equals("")) {
                    this.launchError((TextView)tvErrors[index], error);
//                    break;
                }
            }

            // Aqui van a ir las demás validaciones extra de las basicas


            // Aqui van a ir las validaciones que tengan que ver con la base de datos
            if (rule.isValUnique()) {
                if (this.validateUnique(rule.getNameRule(), this.getData(rule.getNameRule()))) {
                    // Si es True, entonces ya existe en la base de datos y por lo tanto no es unico
                    this.launchError((TextView)tvErrors[index], val.getError("unique").replace("#####", this.getData(rule.getNameRule())));
                }
            }

            index++;
        }

        return this.success;
    }

    /**
     *
     * @param error Indica el componente del error
     * @param message Indica el mensaje del error a mostrar
     */
    private void launchError (TextView error, String message) {
        if (this.success)
            this.success = false;

        error.setText(message);
    }

    /**
     *
     * @param ruleName Indica el nombre del dato a aplicar la validación
     * @return String del dato a validar
     */
    public abstract String getData (String ruleName);

    public abstract boolean validateUnique (String col, String data);

}
