package medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins;

import android.content.Context;
import android.widget.TextView;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.form.FORM_circle_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.table.TABLE_circle_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle_ACTI;
import medqua.jrs.seyer.hakyn.medqua.mixins.form.MIX_FORM_resetFields;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 09:01 AM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_circle_RESETS extends MIX_FORM_resetFields {

    public static void resetErrors (Context ctx) {
        FORM_circle_COMP circleFormCOMP = FORM_circle_COMP.init(null, null);

        TextView [] errors = new TextView[] {
            circleFormCOMP.getTv_errorCircle()
        };

        MIX_FORM_resetFields.resetFields(errors);
    }

    public static void resetForm (Context ctx) {
        FORM_circle_COMP circleFormCOMP = FORM_circle_COMP.init(null, null);

        Object [] fields = new Object[] {
            circleFormCOMP.getEt_circle()
        };

        MIX_FORM_resetFields.resetFields(fields);

        MIX_circle_RESETS.resetErrors(ctx);
        MIX_circle_FIELDS.blockActionModule(ctx);
    }

    public static void resetAllVariablesFromMyActivity (Context ctx) {
        DATA_circle_ACTI circleVARS = MIX_circle_VARIABLES.getVariablesFromMyActivity(ctx);

        circleVARS.setCircleTemp(null);
        circleVARS.setTypeRegister("TVS_NEW");
    }

    public static void resetTable () {
        TABLE_circle_COMP tableCOMP = TABLE_circle_COMP.init(null, null);

        tableCOMP.getLl_table().removeAllViews();
    }
}
