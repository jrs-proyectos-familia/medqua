package medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.form.FORM_circle_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.tvs.rules.RULE_circle_CIRCLE;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle_ACTI;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_rules;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 01:58 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_circle_REGISTERS {

    public static Object [] dataTVS (Context ctx) {
        Object [] fields;
        TextView [] errors;
        FRAME_rules [] rules;

        DATA_circle_ACTI circleVARS = MIX_circle_VARIABLES.getVariablesFromMyActivity(ctx);
        FORM_circle_COMP formCOMP = FORM_circle_COMP.init(null, null);

        fields = new Object[] {
            formCOMP.getEt_circle()
        };

        errors = new TextView[] {
            formCOMP.getTv_errorCircle()
        };

        rules = new FRAME_rules[] {
            new RULE_circle_CIRCLE()
        };

        if (circleVARS.getCircleTemp() != null) {
            DATA_circle newCircle = new DATA_circle();
            newCircle.set_circle(formCOMP.getEt_circle().getText().toString());

            boolean valCircle = newCircle.get_circle().equals(circleVARS.getCircleTemp().get_circle());

            if (valCircle) {
                Toast.makeText(ctx, R.string.toast_dataTemp_noChanges, Toast.LENGTH_SHORT).show();

                return null;
            }
        }

        switch (circleVARS.getTypeRegister()) {
            case "TVS_NEW":
                return new Object[] {
                        fields,
                        errors,
                        rules
                };
            case "TVS_UPDATE":
                return new Object[] {
                        fields,
                        errors,
                        rules
                };
            default:
                return null;
        }
    }

}
