package medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.panel;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.view.View;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.FRAG_zone_FORM;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.FRAG_zone_TABLE;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_FIELDS;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_PHOTO;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_RESETS;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_TABLE;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_menuFragment;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_singleton;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone_ACTI;

import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 5/03/19 a las 10:33 AM
 * @license Joaquin Reyes Sanchez
 */

public class PANEL_zone_LIST implements FRAME_singleton {

    private static PANEL_zone_LIST lisZonePanel = null;

    private Context ctxZonePanel;

    private Activity activity;
    private DATA_zone_ACTI argsActivity;
    private FRAME_menuFragment menuActivity;

    private PANEL_zone_COMP compZonePanel;

    public static PANEL_zone_LIST init (Context ctx, PANEL_zone_COMP compZonePanel) {
        if (PANEL_zone_LIST.lisZonePanel == null)
            PANEL_zone_LIST.lisZonePanel = new PANEL_zone_LIST(ctx, compZonePanel);

        return PANEL_zone_LIST.lisZonePanel;
    }

    private PANEL_zone_LIST(Context ctx, PANEL_zone_COMP comp_zonePanel) {
        this.ctxZonePanel = ctx;
        this.compZonePanel = comp_zonePanel;

        Activity activity = ((Activity) PANEL_zone_LIST.this.ctxZonePanel);
        this.activity = activity;
        this.argsActivity = (DATA_zone_ACTI)activity;
        this.menuActivity = (FRAME_menuFragment)activity;

        new Panel();
    }

    private Fragment currentFragment () {
        return  PANEL_zone_LIST.this.activity.getFragmentManager().findFragmentById(R.id.zone_content);
    }

    private class Panel {
        private Panel () {
            this.newZone();
            this.tabZone();
        }

        private void newZone () {
            PANEL_zone_LIST
                    .this
                    .compZonePanel
                    .getBtn_newZone()
                    .setOnClickListener((View v) -> {
                        if (PANEL_zone_LIST.this.argsActivity.getTypeRegister().equals("TVS_NEW")) {
                            if (PANEL_zone_LIST.this.currentFragment() instanceof FRAG_zone_FORM) {
                                MIX_zone_PHOTO.removePhoto(PANEL_zone_LIST.this.ctxZonePanel, false);
                                MIX_zone_RESETS.resetForm(PANEL_zone_LIST.this.ctxZonePanel);
                            } else PANEL_zone_LIST.this.menuActivity.menuFragment("MENU_FORM");

                            MIX_zone_RESETS.allVariablesFromMyActivity(PANEL_zone_LIST.this.ctxZonePanel);
                        }
                    });
        }

        private void tabZone () {
            PANEL_zone_LIST
                    .this
                    .compZonePanel
                    .getBtn_tabZone()
                    .setOnClickListener((View v) -> {
                        PANEL_zone_LIST.this.argsActivity.setTypeRegister("TVS_NEW");
                        PANEL_zone_LIST.this.argsActivity.setStateThreadTableImages(false);
                        MIX_zone_FIELDS.blockActionPanel(PANEL_zone_LIST.this.ctxZonePanel);

                        if (PANEL_zone_LIST.this.currentFragment() instanceof FRAG_zone_TABLE) {
                            MIX_zone_RESETS.resetTable();
                            ArrayList<DATA_zone> allZones = MIX_zone_TABLE.chargeTable(PANEL_zone_LIST.this.ctxZonePanel);
                            MIX_zone_TABLE.chargeImagesTable(PANEL_zone_LIST.this.ctxZonePanel, allZones);

                            MIX_zone_RESETS.allVariablesFromMyActivity(PANEL_zone_LIST.this.ctxZonePanel);
                        } else PANEL_zone_LIST.this.menuActivity.menuFragment("MENU_TABLE");
                    });
        }
    }


    @Override
    public void clearSingleton() {
        PANEL_zone_LIST.lisZonePanel = null;
    }
}
