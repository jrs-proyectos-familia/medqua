package medqua.jrs.seyer.hakyn.medqua.bd.request;

import android.content.Context;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle;
import medqua.jrs.seyer.hakyn.medqua.bd.BD_management;

import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 07:43 PM
 * @license Joaquin Reyes Sanchez
 */

public class REQ_circle {

    public static ArrayList<DATA_circle> getAllCircles (Context ctx) {
        BD_management manaBD = BD_management.init();

        return manaBD.DAO_Circle(ctx).read();
    }

}
