package medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.panel.PANEL_circle_COMP;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 11:45 AM
 * @license Joaquin Reyes Sanchez
 */

public class FRAG_circle_PANEL extends Fragment {

    private Context ctx;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.circle_panel, container, false);

        this.ctx = view.getContext();
        this.view = view;

        PANEL_circle_COMP.init(this.ctx, this.view);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        PANEL_circle_COMP.init(null, null).clearSingleton();
    }
}
