package medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins;

import android.content.Context;
import android.view.View;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.table.TABLE_ROW_circle_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.table.TABLE_circle_COMP;
import medqua.jrs.seyer.hakyn.medqua.bd.request.REQ_circle;

import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 07:39 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_circle_TABLE {

    public static void chargeTable (Context ctx) {
        ArrayList<DATA_circle> allCircles = REQ_circle.getAllCircles(ctx);

        TABLE_circle_COMP tableCOMP = TABLE_circle_COMP.init(null, null);

        for (DATA_circle circle : allCircles) {
            View view = View.inflate(ctx, R.layout.list_basic_a, null);

            TABLE_ROW_circle_COMP rowCOMP = new TABLE_ROW_circle_COMP(ctx, view);

            // Cargo las imagenes para los botónes de la lista
            rowCOMP.getBtnimg_edit().setImageDrawable(ctx.getResources().getDrawable(R.drawable.img_editar));
            rowCOMP.getBtnimg_delete().setImageDrawable(ctx.getResources().getDrawable(R.drawable.img_basura));

            rowCOMP.getTv_title().setText(circle.get_circle());

            rowCOMP.getBtnimg_edit().setTag(circle);
            rowCOMP.getBtnimg_delete().setTag(circle);

            tableCOMP.getLl_table().addView(view);
        }


    }

}
