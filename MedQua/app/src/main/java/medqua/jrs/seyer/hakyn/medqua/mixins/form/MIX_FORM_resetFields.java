package medqua.jrs.seyer.hakyn.medqua.mixins.form;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 4/03/19 a las 03:02 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_FORM_resetFields {

    protected static void resetFields (Object [] fields) {

        for (Object field: fields) {
            if (field instanceof EditText)
                ((EditText) field).setText("");
            else if (field instanceof TextView)
                ((TextView) field).setText("");
            else if (field instanceof ImageView) {
                ((ImageView) field).setImageBitmap(null);
                ((ImageView) field).setTag("");
            }
        }

    }

}
