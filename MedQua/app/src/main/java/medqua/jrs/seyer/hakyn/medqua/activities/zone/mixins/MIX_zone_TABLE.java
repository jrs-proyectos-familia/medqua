package medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.table.TABLE_ROW_zone_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.table.TABLE_zone_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone_ACTI;
import medqua.jrs.seyer.hakyn.medqua.bd.request.REQ_zone;
import medqua.jrs.seyer.hakyn.medqua.mixins.photo.MIX_PHOT_fixPhoto;
import medqua.jrs.seyer.hakyn.medqua.mixins.photo.MIX_PHOT_getMemory;

import java.io.File;
import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 11/03/19 a las 04:08 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_zone_TABLE {

    public static ArrayList<DATA_zone> chargeTable (Context ctx) {
        ArrayList<DATA_zone> allZones = REQ_zone.getAllZones(ctx);

        TABLE_zone_COMP compZoneTable = TABLE_zone_COMP.init(null, null);

        for (DATA_zone zone : allZones) {
            View child = View.inflate(ctx, R.layout.list_image_basic_a, null);

            TABLE_ROW_zone_COMP compRow = new TABLE_ROW_zone_COMP(ctx, child);

            // Cargo las imagenes para los tres botones de la lista
            compRow.getBtnimg_editZone().setImageDrawable(ctx.getResources().getDrawable(R.drawable.img_editar));
            compRow.getBtnimg_editImage().setImageDrawable(ctx.getResources().getDrawable(R.drawable.img_editar));
            compRow.getBtnimg_delete().setImageDrawable(ctx.getResources().getDrawable(R.drawable.img_basura));

            compRow.getTl_zoneTitle().setText(zone.get_zone());
            String infoTitle = ctx.getResources().getString(R.string.data_id) +  ":";
            compRow.getTl_zoneInfoTitle().setText(infoTitle);
            compRow.getTl_zoneInfoId().setText(zone.get_id().toString());

            compRow.getBtnimg_editZone().setTag(zone);
            compRow.getBtnimg_editImage().setTag(zone);
            compRow.getBtnimg_delete().setTag(zone);

            compZoneTable.getLl_list().addView(child);
        }

        return allZones;
    }

    public static void chargeImagesTable (Context ctx, ArrayList<DATA_zone> allZones) {
        DATA_zone_ACTI zoneVARS = MIX_zone_VARIABLES.getVariablesFromMyActivity(ctx);

        new Thread (() -> {
            File memory = MIX_PHOT_getMemory.sdCard(ctx);
            if (memory.exists()) {
                int index = 0;
                for (DATA_zone zone : allZones) {
                    if (!zoneVARS.getStateThreadTableImages()) {
                        File photo = new File(memory, zone.get_image());

                        Bitmap photoBit = MIX_PHOT_fixPhoto.scalePhoto(photo.getAbsolutePath(), 75, 75);

                        if (photoBit != null) {
                            if (!zoneVARS.getStateThreadTableImages()) {
                                TABLE_zone_COMP comp_zoneTable = TABLE_zone_COMP.init(null, null);

                                int finalIndex = index;
                                comp_zoneTable.getLl_list().post(() -> {
                                    LinearLayout ll = (LinearLayout) comp_zoneTable.getLl_list().getChildAt(finalIndex);
                                    ImageView ivImage = ll.findViewById(R.id.list_imageBasic_A_image);

                                    ivImage.setImageBitmap(photoBit);
                                    ivImage.setTag(zone.get_image());
                                });
                            } else break;
                        }
                    } else break;

                    index++;
                }
            }
        }).start();
    }

}
