package medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.table;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_RESETS;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_TABLE;
import medqua.jrs.seyer.hakyn.medqua.bd.BD_management;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_menuFragment;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone_ACTI;
import medqua.jrs.seyer.hakyn.medqua.mixins.photo.MIX_PHOT_removePhoto;
import medqua.jrs.seyer.hakyn.medqua.mixins.modals.MIX_MODA_previewImage;
import medqua.jrs.seyer.hakyn.medqua.mixins.modals.MIX_MODA_removeRegister;

import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 6/03/19 a las 03:01 PM
 * @license Joaquin Reyes Sanchez
 */

public class TABLE_ROW_zone_LIST {

    private Context ctx;

    private TABLE_ROW_zone_COMP comp;

    private FRAME_menuFragment menuActivity;
    private DATA_zone_ACTI argsActivity;

    public TABLE_ROW_zone_LIST(TABLE_ROW_zone_COMP comp, Context ctx) {
        this.ctx = ctx;
        this.comp = comp;

        Activity activity = (Activity) TABLE_ROW_zone_LIST.this.ctx;
        this.argsActivity = (DATA_zone_ACTI) activity;
        this.menuActivity = (FRAME_menuFragment) activity;

        new Image();
        new Controls();
    }

    private class Image {
        private Image () {
            this.image();
        }

        private void image () {
            TABLE_ROW_zone_LIST
                    .this
                    .comp
                    .getIv_zoneImage()
                    .setOnClickListener((View v) -> {
                        if (!v.getTag().equals("")) {
                            AlertDialog.Builder dialog = MIX_MODA_previewImage.preview(TABLE_ROW_zone_LIST.this.ctx, (String) v.getTag());
                            dialog.show();
                        }
                    });
        }
    }

    private class Controls {
        private Controls () {
            this.editZone();
            this.editImage();
            this.deleteReg();
        }

        private void editZone () {
            TABLE_ROW_zone_LIST
                    .this
                    .comp
                    .getBtnimg_editZone()
                    .setOnClickListener((View v) -> {
                        TABLE_ROW_zone_LIST.this.argsActivity.setZoneTemp((DATA_zone) v.getTag());
                        TABLE_ROW_zone_LIST.this.argsActivity.setTypeRegister("TVS_UPDATE_ZONE");

                        TABLE_ROW_zone_LIST.this.menuActivity.menuFragment("MENU_FORM");
                    });
        }

        private void editImage () {
            TABLE_ROW_zone_LIST
                    .this
                    .comp
                    .getBtnimg_editImage()
                    .setOnClickListener((View v) -> {
                        TABLE_ROW_zone_LIST.this.argsActivity.setZoneTemp((DATA_zone) v.getTag());
                        TABLE_ROW_zone_LIST.this.argsActivity.setTypeRegister("TVS_UPDATE_IMAGE");

                        TABLE_ROW_zone_LIST.this.menuActivity.menuFragment("MENU_FORM");
                    });
        }

        private void deleteReg () {
            TABLE_ROW_zone_LIST
                    .this
                    .comp
                    .getBtnimg_delete()
                    .setOnClickListener((View v) -> {
                        // Abrir un Modal de advertencia al eliminar el registro
                        TABLE_ROW_zone_LIST.this.argsActivity.setZoneTemp((DATA_zone) v.getTag());

                        MIX_MODA_removeRegister<DATA_zone> removeRegister = new MIX_MODA_removeRegister<DATA_zone>() {
                            @Override
                            public void removeRegister(Context ctx, DATA_zone data, AlertDialog dialog) {
                                dialog.dismiss();

                                BD_management manaBD = BD_management.init();
                                if (manaBD.DAO_Zone(TABLE_ROW_zone_LIST.this.ctx).delete(data)) {
                                    MIX_zone_RESETS.resetTable();
                                    ArrayList<DATA_zone> allZones = MIX_zone_TABLE.chargeTable(TABLE_ROW_zone_LIST.this.ctx);
                                    MIX_zone_TABLE.chargeImagesTable(TABLE_ROW_zone_LIST.this.ctx, allZones);

                                    MIX_PHOT_removePhoto.remove(data.get_image(), ctx, false);
                                }
                            }
                        };
                        removeRegister.dialogRemove(TABLE_ROW_zone_LIST.this.ctx, TABLE_ROW_zone_LIST.this.argsActivity.getZoneTemp());
                    });
        }
    }

}
