package medqua.jrs.seyer.hakyn.medqua.activities.circle.tvs;

import android.content.Context;
import android.widget.TextView;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_VARIABLES;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle_ACTI;
import medqua.jrs.seyer.hakyn.medqua.bd.BD_management;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_rules;
import medqua.jrs.seyer.hakyn.medqua.tvs.TVS_treatment;
import medqua.jrs.seyer.hakyn.medqua.tvs.TVS_validation;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 02:11 PM
 * @license Joaquin Reyes Sanchez
 */

public class TVS_circle implements TVS_circle_BASE {
    private DATA_circle dataTreat = null;

    public Object [] fields;
    public TextView [] errors;
    public FRAME_rules [] rules;

    private Context ctx = null;

    private DATA_circle_ACTI circleVARS;

    @Override
    public void treatment() {
        TVS_treatment<DATA_circle> treatment = new TVS_treatment<DATA_circle>() {
            @Override
            public DATA_circle buildingData(String[] treatData) {
                DATA_circle dataCircle = new DATA_circle();

                int index = 0;

                for (FRAME_rules rule : TVS_circle.this.rules) {
                    String nameMethod = "set_" + rule.getNameRule();

                    for (Method method : dataCircle.getClass().getDeclaredMethods()) {
                        if (nameMethod.equals(method.getName())) {
                            try {
                                Method methodSet = dataCircle.getClass().getMethod(nameMethod, rule.getTypeRule());
                                methodSet.invoke(dataCircle, treatData[index]);
                            } catch (NoSuchMethodException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }

                            break;
                        }
                    }

                    index++;
                }

                return dataCircle;
            }
        };

        this.dataTreat = treatment.treatment(this.fields, this.rules);
    }

    @Override
    public boolean validation() {
        this.treatment();

        TVS_validation validation = new TVS_validation() {
            @Override
            public String getData(String ruleName) {
                String data = "";

                String methodRule = "get_" + ruleName;

                for (Method method : TVS_circle.this.dataTreat.getClass().getDeclaredMethods()) {
                    if (method.getName().equals(methodRule)) {
                        try {
                            Method dataMethod = TVS_circle.this.dataTreat.getClass().getMethod(methodRule);
                            if (dataMethod.getReturnType() == String.class)
                                data = (String) dataMethod.invoke(TVS_circle.this.dataTreat);
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                }

                return data;
            }

            @Override
            public boolean validateUnique(String col, String data) {
                BD_management manaBD = BD_management.init();

                String sql = "SELECT id FROM circle WHERE "+ col + " = \"" + data +"\"";

                return manaBD.DAO_Circle(TVS_circle.this.ctx).valUnique(sql);
            }
        };

        return validation.validate(this.errors, this.rules, this.ctx);
    }

    @Override
    public boolean server(Context ctx) {
        this.ctx = ctx;

        if (this.validation()) {
            this.circleVARS = MIX_circle_VARIABLES.getVariablesFromMyActivity(this.ctx);

            BD_management manaBD = BD_management.init();

            switch (this.circleVARS.getTypeRegister()) {
                case "TVS_NEW":
                    return manaBD.DAO_Circle(ctx).create(this.dataTreat);
                case "TVS_UPDATE":
                    this.dataTreat.set_id(this.circleVARS.getCircleTemp().get_id());
                    return manaBD.DAO_Circle(ctx).update(this.dataTreat);
            }
        }

        return false;
    }
}
