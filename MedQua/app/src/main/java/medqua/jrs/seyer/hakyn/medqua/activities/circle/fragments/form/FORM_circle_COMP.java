package medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.form;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_singleton;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 12/03/19 a las 07:10 PM
 * @license Joaquin Reyes Sanchez
 */

public class FORM_circle_COMP implements FRAME_singleton {
    private static FORM_circle_COMP compCircle = null;

    public static FORM_circle_COMP init (Context ctx, View v) {
        if (FORM_circle_COMP.compCircle == null)
            FORM_circle_COMP.compCircle = new FORM_circle_COMP(ctx, v);

        return FORM_circle_COMP.compCircle;
    }

    private EditText et_circle;
    private Button btn_send;
    private ImageButton btnImg_clean, btnImg_newCircle;
    private TextView tv_errorCircle;

    private FORM_circle_COMP(Context ctx, View v) {
        this.et_circle = v.findViewById(R.id.form_circle_input_circle);
        this.btn_send = v.findViewById(R.id.form_control_sendData);
        this.btnImg_clean = v.findViewById(R.id.form_control_cleanForm);
        this.btnImg_newCircle = v.findViewById(R.id.form_control_newRegister);
        this.tv_errorCircle = v.findViewById(R.id.form_circle_error_circle);

        FORM_circle_LIST.init(ctx, this);
    }

    @Override
    public void clearSingleton() {
        FORM_circle_COMP.compCircle = null;

        FORM_circle_LIST.init(null, null).clearSingleton();
    }

    public EditText getEt_circle() {
        return et_circle;
    }

    public Button getBtn_send() {
        return btn_send;
    }

    public ImageButton getBtnImg_clean() {
        return btnImg_clean;
    }

    public ImageButton getBtnImg_newCircle() {
        return btnImg_newCircle;
    }

    public TextView getTv_errorCircle() {
        return tv_errorCircle;
    }
}
