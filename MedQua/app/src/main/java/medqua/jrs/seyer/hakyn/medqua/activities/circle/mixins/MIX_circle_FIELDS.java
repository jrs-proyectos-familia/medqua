package medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.form.FORM_circle_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.panel.PANEL_circle_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle_ACTI;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 09:28 AM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_circle_FIELDS {

    public static void blockActionPanel(Context ctx) {
        DATA_circle_ACTI circleVARS = MIX_circle_VARIABLES.getVariablesFromMyActivity(ctx);

        PANEL_circle_COMP panelCOMP = PANEL_circle_COMP.init(null, null);

        String panNewCircle_title = ctx.getResources().getString(R.string.circle_panel_newRegister);

        switch (circleVARS.getTypeRegister()) {
            case "TVS_UPDATE":
                panNewCircle_title = ctx.getResources().getString(R.string.circle_panel_editRegister);
                break;
        }

        panelCOMP.getBtn_newCircle().setText(panNewCircle_title);
    }

    public static void blockActionModule(Context ctx) {
        DATA_circle_ACTI circleVARS = MIX_circle_VARIABLES.getVariablesFromMyActivity(ctx);

        FORM_circle_COMP circleFormCOMP = FORM_circle_COMP.init(null, null);

        MIX_circle_FIELDS.blockActionPanel(ctx);

        boolean formCircle_enable = true;
        int formCircle_color = ctx.getResources().getColor(R.color.main_h);
        Drawable formCircle_style = ctx.getResources().getDrawable(R.drawable.style_inputs);

        String contSend_text = ctx.getResources().getString(R.string.action_send);
        int contSend_color = ctx.getResources().getColor(R.color.main_h);

        int contNewReg_visible = View.INVISIBLE;

        switch (circleVARS.getTypeRegister()) {
            case "TVS_UPDATE":
                contSend_text = ctx.getResources().getString(R.string.action_edit);
                contSend_color = ctx.getResources().getColor(R.color.secondary_yellow);

                contNewReg_visible = View.VISIBLE;
                break;
        }

        int paddingInputs = (int) ctx.getResources().getDimension(R.dimen.padding_df);

        circleFormCOMP.getEt_circle().setEnabled(formCircle_enable);
        circleFormCOMP.getEt_circle().setTextColor(formCircle_color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            circleFormCOMP.getEt_circle().setBackground(formCircle_style);
        }
        circleFormCOMP.getEt_circle().setPadding(paddingInputs, paddingInputs, paddingInputs, paddingInputs);

        circleFormCOMP.getBtn_send().setText(contSend_text);
        circleFormCOMP.getBtn_send().setBackgroundColor(contSend_color);

        circleFormCOMP.getBtnImg_newCircle().setVisibility(contNewReg_visible);
    }

    public static void chargeFieldsWithDataTemp(Context ctx) {
        DATA_circle_ACTI circleVARS = MIX_circle_VARIABLES.getVariablesFromMyActivity(ctx);

        FORM_circle_COMP circleCOMP = FORM_circle_COMP.init(null, null);

        if (circleVARS.getCircleTemp() != null)
            circleCOMP.getEt_circle().setText(circleVARS.getCircleTemp().get_circle());
    }
}
