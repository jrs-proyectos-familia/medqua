package medqua.jrs.seyer.hakyn.medqua.activities.circle.dao;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 04:17 PM
 * @license Joaquin Reyes Sanchez
 */

public interface DAO_circle_SQL {

    String sql_readAllCircle = "SELECT * FROM circle ORDER BY circle ASC";
}
