package medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.form;

import android.content.Context;
import android.view.View;
import android.widget.*;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_singleton;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 1/03/19 a las 11:59 AM
 * @license Joaquin Reyes Sanchez
 */

public class FORM_zone_COMP implements FRAME_singleton {


    private static FORM_zone_COMP compZone = null;

    private EditText et_zone;
    private Button btn_image, btn_send;
    private ImageButton btnimg_clean, btnimg_newZone;
    private ImageView iv_image;
    private TextView tv_error_zone, tv_error_imageZone;

    public static FORM_zone_COMP init (Context ctx, View v) {
        if (FORM_zone_COMP.compZone == null)
            FORM_zone_COMP.compZone = new FORM_zone_COMP(ctx, v);

        return FORM_zone_COMP.compZone;
    }

    private FORM_zone_COMP(Context ctx, View v) {
        this.et_zone = v.findViewById(R.id.form_zone_input_zone);
        this.btn_image = v.findViewById(R.id.form_zone_action_image);
        this.btn_send = v.findViewById(R.id.form_control_sendData);
        this.btnimg_clean = v.findViewById(R.id.form_control_cleanForm);
        this.btnimg_newZone = v.findViewById(R.id.form_control_newRegister);
        this.iv_image = v.findViewById(R.id.form_zone_input_image);
        this.iv_image.setTag("");
        this.tv_error_zone = v.findViewById(R.id.form_zone_error_zone);
        this.tv_error_imageZone = v.findViewById(R.id.form_zone_error_image);

        FORM_zone_LIST.init(ctx, this);
    }

    @Override
    public void clearSingleton() {
        FORM_zone_COMP.compZone = null;
        FORM_zone_LIST.init(null, null).clearSingleton();
    }

    public EditText getEt_zone() {
        return et_zone;
    }

    public Button getBtn_image() {
        return btn_image;
    }

    public ImageView getIv_image() {
        return iv_image;
    }

    public Button getBtn_send() {
        return btn_send;
    }

    public TextView getTv_error_zone() {
        return tv_error_zone;
    }

    public TextView getTv_error_imageZone() {
        return tv_error_imageZone;
    }

    public ImageButton getBtnimg_clean() {
        return btnimg_clean;
    }

    public ImageButton getBtnimg_newZone() {
        return btnimg_newZone;
    }
}
