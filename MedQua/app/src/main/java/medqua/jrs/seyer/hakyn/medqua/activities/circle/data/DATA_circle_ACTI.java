package medqua.jrs.seyer.hakyn.medqua.activities.circle.data;

import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_variablesFragment;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 12/03/19 a las 07:30 PM
 * @license Joaquin Reyes Sanchez
 */

public interface DATA_circle_ACTI extends FRAME_variablesFragment {

    DATA_circle getCircleTemp();
    void setCircleTemp(DATA_circle circle);

}
