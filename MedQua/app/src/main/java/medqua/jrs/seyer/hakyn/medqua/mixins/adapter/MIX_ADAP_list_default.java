package medqua.jrs.seyer.hakyn.medqua.mixins.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 5/03/19 a las 09:35 PM
 * @license Joaquin Reyes Sanchez
 */

public abstract class MIX_ADAP_list_default<T> extends BaseAdapter {

    private static LayoutInflater inflater = null;

    private Context ctx;
    private ArrayList<T> data;

    public MIX_ADAP_list_default(Context ctx, ArrayList<T> data) {
        this.ctx = ctx;
        this.data = data;

        MIX_ADAP_list_default.inflater = (LayoutInflater)this.ctx.getSystemService(this.ctx.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public abstract View getView(int position, View convertView, ViewGroup parent);

    public ArrayList<T> getData() {
        return data;
    }


}
