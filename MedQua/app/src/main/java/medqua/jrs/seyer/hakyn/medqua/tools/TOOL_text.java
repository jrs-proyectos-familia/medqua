package medqua.jrs.seyer.hakyn.medqua.tools;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 1/03/19 a las 06:36 PM
 * @license Joaquin Reyes Sanchez
 */

public class TOOL_text {

    /**
     *
     * @param size Indica el tamaño que tendrá la cadena deseada
     * @return String
     */
    public static String randomChain (int size) {
        String data = "qwezxcdsarfvbgtyhnmjuklpoiMNBVCXZQWERTYUIOPLAKSJDHFG1092837456";

        String chain = "";

        for (int i = 0; i < size; i++)
            chain += data.charAt((int) (Math.random() * size));

        return chain;
    }

    /**
     *
     * @param pattern Caracteres a buscar y reemplazar
     * @param replaceWith Caracteres sustitutos
     * @param inputString Texto original
     * @return
     */
    public static String findAndReplaceAll(String pattern, String replaceWith, String inputString) {
        Pattern search = Pattern.compile(pattern);
        Matcher matcher = search.matcher(inputString);

        return matcher.replaceAll(replaceWith);
    }

}
