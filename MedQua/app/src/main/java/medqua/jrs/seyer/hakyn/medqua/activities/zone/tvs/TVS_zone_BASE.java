package medqua.jrs.seyer.hakyn.medqua.activities.zone.tvs;

import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_tvs;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 4/03/19 a las 12:00 PM
 * @license Joaquin Reyes Sanchez
 */

public interface TVS_zone_BASE extends FRAME_tvs {
}
