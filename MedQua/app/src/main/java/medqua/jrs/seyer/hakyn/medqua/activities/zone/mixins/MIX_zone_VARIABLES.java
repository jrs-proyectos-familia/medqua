package medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins;

import android.app.Activity;
import android.content.Context;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone_ACTI;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 11:36 AM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_zone_VARIABLES {

    public static DATA_zone_ACTI getVariablesFromMyActivity (Context ctx) {
        Activity actiZone = (Activity) ctx;

        return (DATA_zone_ACTI) actiZone;
    }

}
