package medqua.jrs.seyer.hakyn.medqua.activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.FRAG_circle_FORM;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.FRAG_circle_TABLE;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.panel.PANEL_circle_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle_ACTI;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_menuFragment;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 12/03/19 a las 06:49 PM
 * @license Joaquin Reyes Sanchez
 */

public class ACTI_circle extends Activity implements FRAME_menuFragment, DATA_circle_ACTI {
    private String typeRegister = "TVS_NEW";
    private DATA_circle circleTemp = null;

    @Override
    protected void onCreate(Bundle instance) {
        super.onCreate(instance);

        this.setContentView(R.layout.circle);

        this.menuFragment("MENU_FORM");
    }

    @Override
    public void menuFragment(String fragment) {
        FragmentManager fm = this.getFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();

        PANEL_circle_COMP panelCOMP = PANEL_circle_COMP.init(null, null);

        switch (fragment) {
            case "MENU_FORM":
                ft.replace(R.id.circle_content, new FRAG_circle_FORM());

                panelCOMP.getBtn_newCircle().setBackgroundColor(getResources().getColor(R.color.main_s));
                panelCOMP.getBtn_tabCircle().setBackgroundColor(getResources().getColor(R.color.main_h));
                break;
            case "MENU_TABLE":
                ft.replace(R.id.circle_content, new FRAG_circle_TABLE());

                panelCOMP.getBtn_newCircle().setBackgroundColor(getResources().getColor(R.color.main_h));
                panelCOMP.getBtn_tabCircle().setBackgroundColor(getResources().getColor(R.color.main_s));
                break;
        }

        ft.commit();
    }

    @Override
    public DATA_circle getCircleTemp() {
        return this.circleTemp;
    }

    @Override
    public void setCircleTemp(DATA_circle circle) {
        this.circleTemp = circle;
    }

    @Override
    public String getTypeRegister() {
        return this.typeRegister;
    }

    @Override
    public void setTypeRegister(String typeAction) {
        this.typeRegister = typeAction;
    }
}
