package medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.form.FORM_zone_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_FIELDS;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_PHOTO;

/**
 *
 * Archivo FRAG_zone_PANEL.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 1/03/19 a las 10:01 AM
 * @license Joaquin Reyes Sanchez
 *
 */

public class FRAG_zone_FORM extends Fragment {

    private Context ctx = null;
    private View view = null;

    public FRAG_zone_FORM() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layouts for this fragment
        View viewForm =  inflater.inflate(R.layout.zone_form, container, false);
        this.ctx = viewForm.getContext();
        this.view = viewForm;

        FORM_zone_COMP.init(this.ctx, viewForm);

        MIX_zone_FIELDS.blockActionZone(this.ctx);

        MIX_zone_FIELDS.chargeRegister(this.ctx);

        return viewForm;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        FORM_zone_COMP compZoneForm = FORM_zone_COMP.init(null, null);

        // Elimino la imagen de la memoria SD antes de que destruya este fragmento. Este es a que si hay una imagen en el ImageView es porque existe ya la imagen SD en la memoria SD y por lo tanto hay que borrarlo en caso de que no se haya realizado el proceso de envio al TVS. Para ello se necesita validar si hay una imagen visualizada y además revisar que estamos en el proceso TVS_NEW debido a que si estamos en otro proceso podremos accidetentalmente eliminar una imagen de algún registro ya éxistente
        MIX_zone_PHOTO.removePhoto(this.ctx, false);

        compZoneForm.clearSingleton();
    }
}
