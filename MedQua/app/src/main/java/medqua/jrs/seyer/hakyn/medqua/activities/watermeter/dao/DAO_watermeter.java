package medqua.jrs.seyer.hakyn.medqua.activities.watermeter.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import medqua.jrs.seyer.hakyn.medqua.activities.watermeter.data.DATA_watermeter;
import medqua.jrs.seyer.hakyn.medqua.bd.BD_sqlite;

import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 27/03/19 a las 08:41 PM
 * @license Joaquin Reyes Sanchez
 */

public class DAO_watermeter implements DAO_watermeter_BASE {
    private Context ctx;
    private BD_sqlite bdSqlite;

    public DAO_watermeter (Context ctx) {
        this.ctx = ctx;
        this.bdSqlite = new BD_sqlite(this.ctx, "medqua", null, 1);
    }

    @Override
    public boolean create(DATA_watermeter data) {
        boolean success = false;

        SQLiteDatabase db = this.bdSqlite.getWritableDatabase();

        ContentValues newWatermeter = new ContentValues();
        newWatermeter.put("number", data.get_number());
        newWatermeter.put("image", data.get_image());
        newWatermeter.put("longitude", data.get_longitude());
        newWatermeter.put("latitude", data.get_latitude());
        newWatermeter.put("notes", data.get_notes());

        if (db.insert("watermeter", null, newWatermeter) != -1) {
            // TODO: Continuar con esto
        }

        return success;
    }

    @Override
    public boolean update(DATA_watermeter data) {
        return false;
    }

    @Override
    public boolean delete(DATA_watermeter data) {
        return false;
    }

    @Override
    public ArrayList<DATA_watermeter> read() {
        return null;
    }

    @Override
    public boolean valUnique(String sql) {
        return false;
    }

    @Override
    public void setContext(Context ctx) {

    }
}
