package medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins;

import android.content.Context;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.form.FORM_zone_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone_ACTI;
import medqua.jrs.seyer.hakyn.medqua.mixins.photo.MIX_PHOT_removePhoto;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 9/03/19 a las 11:14 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_zone_PHOTO {

    /**
     *
     * @param ctx Indica el contexto de la Activity
     * @param showToast Indica si se va a mostrar un Toast cuando la imagen ha sido eliminada
     */
    public static void removePhoto (Context ctx, boolean showToast) {
        FORM_zone_COMP compZoneForm = FORM_zone_COMP.init(null, null);

        String routeImage = (String) compZoneForm.getIv_image().getTag();

        DATA_zone_ACTI argsActivity = MIX_zone_VARIABLES.getVariablesFromMyActivity(ctx);

        if (argsActivity.getZoneTemp() == null) {
            if (!routeImage.equals(""))
                if (MIX_PHOT_removePhoto.remove(routeImage, ctx, showToast))
                    compZoneForm.getIv_image().setTag("");
        } else {
            String routeImageTemp = argsActivity.getZoneTemp().get_image();
            if (!routeImage.equals(routeImageTemp))
                if (MIX_PHOT_removePhoto.remove(routeImage, ctx, showToast))
                    compZoneForm.getIv_image().setTag("");
        }
    }

}
