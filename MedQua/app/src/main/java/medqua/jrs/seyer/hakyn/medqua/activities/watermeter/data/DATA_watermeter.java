package medqua.jrs.seyer.hakyn.medqua.activities.watermeter.data;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 27/03/19 a las 08:27 PM
 * @license Joaquin Reyes Sanchez
 */

public class DATA_watermeter {

    private Integer id = -1;
    private String number = "";
    private String image = "";
    private String longitude = "";
    private String latitude = "";
    private String notes = "";

    public DATA_watermeter (Integer id, String number, String image, String longitude, String latitude, String notes) {
        this.id = id;
        this.number = number;
        this.image = image;
        this.longitude = longitude;
        this.latitude = latitude;
        this.notes = notes;
    }

    public DATA_watermeter () {}

    public Integer get_id() {
        return id;
    }

    public void set_id(Integer id) {
        this.id = id;
    }

    public String get_number() {
        return number;
    }

    public void set_number(String number) {
        this.number = number;
    }

    public String get_image() {
        return image;
    }

    public void set_image(String image) {
        this.image = image;
    }

    public String get_longitude() {
        return longitude;
    }

    public void set_longitude(String longitude) {
        this.longitude = longitude;
    }

    public String get_latitude() {
        return latitude;
    }

    public void set_latitude(String latitude) {
        this.latitude = latitude;
    }

    public String get_notes() {
        return notes;
    }

    public void set_notes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        return "DATA_watermeter{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", image='" + image + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", notes='" + notes + '\'' +
                '}';
    }
}
