package medqua.jrs.seyer.hakyn.medqua.frame;

import android.content.Context;

import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 3/03/19 a las 10:30 PM
 * @license Joaquin Reyes Sanchez
 */

public interface FRAME_dao<T> {

    /**
     *
     * @param data Indica la clase contenedora de todos los datos ya tratados y listos para subir a la base de datos
     * @return boolean True -> Registro creado satisfactoriamente; False -> Registro no creado
     */
    boolean create (T data);

    /**
     *
     * @param data Indica la clase contenedora de todos los datos ya tratados y listos para ser actualizados
     * @return boolean True -> Registro actualizado satisfactoriamente; False -> Registro no actualizado
     */
    boolean update (T data);

    /**
     *
     * @param data Indica la clase contenedora de todos los datos ya tratados y listos para ser usados en la eliminación del registro
     * @return boolean True -> Registro eliminado satisfactoriamente; False -> Registro no eliminado
     */
    boolean delete (T data);

    ArrayList<T> read ();

    /**
     *
     * @param sql Indica el codigo sql a inyectar para realizar la validación en la base de datos
     * @return boolean True -> La Zona ya existe; False -> La Zona no exite
     */
    boolean valUnique (String sql);

    void setContext (Context ctx);

}
