package medqua.jrs.seyer.hakyn.medqua.activities.circle.data;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 12/03/19 a las 07:30 PM
 * @license Joaquin Reyes Sanchez
 */

public class DATA_circle {

    private Integer id = -1;
    private String circle = "";

    public DATA_circle(Integer id, String circle) {
        this.id = id;
        this.circle = circle;
    }

    public DATA_circle() {}

    public Integer get_id() {
        return id;
    }

    public void set_id(Integer id) {
        this.id = id;
    }

    public String get_circle() {
        return circle;
    }

    public void set_circle(String circle) {
        this.circle = circle;
    }

    @Override
    public String toString() {
        return "DATA_circle{" +
                "id=" + id +
                ", circle='" + circle + '\'' +
                '}';
    }
}
