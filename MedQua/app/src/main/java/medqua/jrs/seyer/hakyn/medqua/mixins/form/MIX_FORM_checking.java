package medqua.jrs.seyer.hakyn.medqua.mixins.form;

import android.content.Context;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import medqua.jrs.seyer.hakyn.medqua.R;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 2/03/19 a las 08:39 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_FORM_checking {

    /**
     *
     * @param fields Indica el conjunto de componentes a evaluar si estan vacios
     * @param tvErrors Indica el conjunto de componentes de errores a los cuales se enviará un mensaje de error. Importante: éste debe de ser del mismo tamaño que el de fields y además tienen que coincidir sus llaves de acceso, es decir la misma posición
     * @return boolean True -> Ha pasado la validación: False -> No ha pasado la Validación
     */
    public static boolean checkEmptyFields (Context ctx, Object [] fields, TextView [] tvErrors) {
        if (tvErrors.length == fields.length) {
            int success = 0;
            int index = 0;

            for (Object field : fields) {
                if (field instanceof EditText) {
                    if ( !((EditText)field).getText().toString().equals("") ) {
                        success++;
                        ((TextView)tvErrors[index]).setText("");
                    } else ((TextView)tvErrors[index]).setText(R.string.validator_empty);
                } else if (field instanceof String) {
                    if ( !field.equals("") ) {
                        success++;
                        ((TextView)tvErrors[index]).setText("");
                    } else ((TextView)tvErrors[index]).setText(R.string.validator_empty);
                }

                index++;
            }

            if (success == fields.length) return true;
            else {
                Toast.makeText(ctx, R.string.toast_form_missingDataForSend, Toast.LENGTH_SHORT).show();
                return false;
            }
        } return false;
    }

}
