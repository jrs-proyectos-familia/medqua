package medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.panel;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.view.View;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.FRAG_circle_FORM;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.FRAG_circle_TABLE;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_FIELDS;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_RESETS;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_TABLE;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_VARIABLES;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle_ACTI;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_menuFragment;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_singleton;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 01:47 PM
 * @license Joaquin Reyes Sanchez
 */

public class PANEL_circle_LIST implements FRAME_singleton {
    private static PANEL_circle_LIST panelLIST = null;

    public static PANEL_circle_LIST init (Context ctx, PANEL_circle_COMP panelCOMP) {
        if (PANEL_circle_LIST.panelLIST == null)
            PANEL_circle_LIST.panelLIST = new PANEL_circle_LIST(ctx, panelCOMP);

        return PANEL_circle_LIST.panelLIST;
    }

    private Context ctx;
    private PANEL_circle_COMP panelCOMP;
    private DATA_circle_ACTI circleVARS;
    private FRAME_menuFragment circleMENU;

    private PANEL_circle_LIST(Context ctx, PANEL_circle_COMP panelCOMP) {
        this.ctx = ctx;
        this.panelCOMP = panelCOMP;
        this.circleVARS = MIX_circle_VARIABLES.getVariablesFromMyActivity(ctx);
        this.circleMENU = MIX_circle_VARIABLES.getMenuFromMyActivity(ctx);

        new Panel();
    }

    private Fragment currentFragment () {
        Activity activity = (Activity) PANEL_circle_LIST.this.ctx;

        return activity.getFragmentManager().findFragmentById(R.id.circle_content);
    }

    private class Panel {
        private Panel () {
            this.newCircle();
            this.tabCircle();
        }

        private void newCircle() {
            PANEL_circle_LIST
                    .this
                    .panelCOMP
                    .getBtn_newCircle()
                    .setOnClickListener((View v) -> {
                        if (PANEL_circle_LIST.this.circleVARS.getTypeRegister().equals("TVS_NEW")) {
                            if (PANEL_circle_LIST.this.currentFragment() instanceof FRAG_circle_FORM) {
                                MIX_circle_RESETS.resetForm(PANEL_circle_LIST.this.ctx);
                            } else PANEL_circle_LIST.this.circleMENU.menuFragment("MENU_FORM");

                            MIX_circle_RESETS.resetAllVariablesFromMyActivity(PANEL_circle_LIST.this.ctx);
                        }
                    });
        }

        private void tabCircle () {
            PANEL_circle_LIST
                    .this
                    .panelCOMP
                    .getBtn_tabCircle()
                    .setOnClickListener((View v) -> {
                        PANEL_circle_LIST.this.circleVARS.setTypeRegister("TVS_NEW");
                        MIX_circle_FIELDS.blockActionPanel(PANEL_circle_LIST.this.ctx);

                        if (PANEL_circle_LIST.this.currentFragment() instanceof FRAG_circle_TABLE) {
                            MIX_circle_RESETS.resetTable();

                            MIX_circle_TABLE.chargeTable(PANEL_circle_LIST.this.ctx);

                            MIX_circle_RESETS.resetAllVariablesFromMyActivity(PANEL_circle_LIST.this.ctx);
                        } else PANEL_circle_LIST.this.circleMENU.menuFragment("MENU_TABLE");
                    });
        }
    }

    @Override
    public void clearSingleton() {
        PANEL_circle_LIST.panelLIST = null;
    }
}
