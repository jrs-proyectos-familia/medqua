package medqua.jrs.seyer.hakyn.medqua.activities.watermeter.dao;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 27/03/19 a las 08:43 PM
 * @license Joaquin Reyes Sanchez
 */

public interface DAO_watermeter_SQL {

    String sql_readAllWatermeters = "SELECT * FROM watermeter ORDER BY number ASC";

}
