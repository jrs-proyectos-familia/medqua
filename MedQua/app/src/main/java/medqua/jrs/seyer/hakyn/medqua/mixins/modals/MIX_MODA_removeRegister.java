package medqua.jrs.seyer.hakyn.medqua.mixins.modals;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import medqua.jrs.seyer.hakyn.medqua.R;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 12/03/19 a las 01:13 PM
 * @license Joaquin Reyes Sanchez
 */

public abstract class MIX_MODA_removeRegister<T> {

    public void dialogRemove (Context ctx, T data) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);

        View view = View.inflate(ctx, R.layout.moda_remove_register, null);

        Button accept = view.findViewById(R.id.modal_removeRegister_remove);
        Button cancel = view.findViewById(R.id.modal_removeRegister_cancel);

        dialog.setView(view);

        AlertDialog showDialog = dialog.show();

        accept.setOnClickListener((View v) -> this.removeRegister(ctx, data, showDialog));

        cancel.setOnClickListener((View v) -> showDialog.dismiss());
    }

    public abstract void removeRegister (Context ctx, T data, AlertDialog dialog);

}
