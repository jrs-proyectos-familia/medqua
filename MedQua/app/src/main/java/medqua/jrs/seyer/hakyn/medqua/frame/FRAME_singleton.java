package medqua.jrs.seyer.hakyn.medqua.frame;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 1/03/19 a las 12:10 PM
 * @license Joaquin Reyes Sanchez
 */

public interface FRAME_singleton {

    void clearSingleton ();

}
