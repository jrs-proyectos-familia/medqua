package medqua.jrs.seyer.hakyn.medqua.mixins.photo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.Toast;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.form.FORM_zone_COMP;
import medqua.jrs.seyer.hakyn.medqua.tools.TOOL_text;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 1/03/19 a las 06:42 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_PHOT_takePhoto extends Activity {

    public static void photo (Context ctx, String category) {
        Intent intentPhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Reviso si existe la targeta SD
        File memory = MIX_PHOT_getMemory.sdCard(ctx);

        if (memory.exists() && memory.canWrite() && memory.canRead()) {
            File dirPhoto = new File(memory, "images/" + category);
            if (!dirPhoto.exists()) dirPhoto.mkdirs();

            String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
            String chain = TOOL_text.randomChain(9);
            String titlePhoto = "HS_" + timeStamp + "_" + chain + ".jpg";

            File filePhoto = new File(dirPhoto, titlePhoto);
            Uri uriPhoto = Uri.fromFile(filePhoto);

            Activity activity = ((Activity)ctx);

            FORM_zone_COMP compZoneForm = FORM_zone_COMP.init(null, null);

            switch (category) {
                case "zone":
                    compZoneForm.getIv_image().setTag("images/" + category + "/" + titlePhoto);
                    break;
            }

            intentPhoto.putExtra(MediaStore.EXTRA_OUTPUT, uriPhoto);
            activity.startActivityForResult(intentPhoto, 1);
        } else
            Toast.makeText(ctx, R.string.toast_image_saveError, Toast.LENGTH_SHORT).show();
    }

    public static String saveImageFromGallery (Context ctx, String category, Bitmap imageBit) {
        String routeImage = "";

        // Reviso si existe la targeta SD
        File memory = MIX_PHOT_getMemory.sdCard(ctx);

        if (memory.exists() && memory.canWrite() && memory.canRead()) {
            File dirPhoto = new File(memory, "images/" + category);
            if (!dirPhoto.exists()) dirPhoto.mkdirs();

            String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
            String chain = TOOL_text.randomChain(9);
            String titlePhoto = "HS_" + timeStamp + "_" + chain + ".jpg";

            File filePhoto = new File (dirPhoto, titlePhoto);
            if (filePhoto.exists()) filePhoto.delete();

            try {
                FileOutputStream out = new FileOutputStream(filePhoto);
                imageBit.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();

                routeImage = "images/" + category + "/" + titlePhoto;

                Toast.makeText(ctx, R.string.toast_image_save, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else
            Toast.makeText(ctx, R.string.toast_image_saveError, Toast.LENGTH_SHORT).show();

        return routeImage;
    }

}
