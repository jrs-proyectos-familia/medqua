package medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.panel.PANEL_zone_COMP;

/**
 *
 * Archivo FRAG_zone_PANEL.java
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 5/03/19 a las 10:01 AM
 * @license Joaquin Reyes Sanchez
 *
 */

public class FRAG_zone_PANEL extends  Fragment {

    private Context ctx = null;
    private View view = null;

    public FRAG_zone_PANEL() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layouts for this fragment
        View view = inflater.inflate(R.layout.zone_panel, container, false);

        this.ctx = view.getContext();
        this.view = view;

        PANEL_zone_COMP.init(this.ctx, view);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        PANEL_zone_COMP.init(null, null).clearSingleton();
    }
}
