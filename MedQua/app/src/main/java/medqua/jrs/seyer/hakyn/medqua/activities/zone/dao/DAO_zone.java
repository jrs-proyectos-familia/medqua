package medqua.jrs.seyer.hakyn.medqua.activities.zone.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone;
import medqua.jrs.seyer.hakyn.medqua.bd.BD_sqlite;

import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 3/03/19 a las 10:45 PM
 * @license Joaquin Reyes Sanchez
 */

public class DAO_zone implements DAO_zone_BASE<DATA_zone> {

    private Context ctx = null;
    private BD_sqlite bdSqlite;

    public DAO_zone(Context ctx) {
        this.ctx = ctx;
        this.bdSqlite = new BD_sqlite(this.ctx, "medqua", null, 1);
    }

    @Override
    public boolean create(DATA_zone data) {
        boolean success = false;

        SQLiteDatabase db = this.bdSqlite.getWritableDatabase();

        ContentValues newZone = new ContentValues();
        newZone.put("zone", data.get_zone());
        newZone.put("image", data.get_image());

        if (db.insert("zone", null, newZone) != -1) {
            String messageToast = this.ctx.getResources().getString(R.string.dataBase_created).replaceAll("#####", data.get_zone());

            Toast.makeText(this.ctx, messageToast, Toast.LENGTH_SHORT).show();

            success = true;
        } else System.out.println("NO SE PUDO GUARDAR");

        db.close();

        return success;
    }

    @Override
    public boolean update(DATA_zone data) {
        return false;
    }

    @Override
    public boolean updateZone(DATA_zone data) {
        boolean success = false;

        SQLiteDatabase db = this.bdSqlite.getWritableDatabase();

        ContentValues editZone = new ContentValues();
        editZone.put("zone", data.get_zone());

        if (db.update("zone", editZone, "id = " + data.get_id(), null) > 0) {
            String messageToast = this.ctx.getResources().getString(R.string.dataBase_updated).replaceAll("#####", data.get_zone());

            Toast.makeText(this.ctx, messageToast, Toast.LENGTH_SHORT).show();

            success = true;
        }

        db.close();

        return success;
    }

    @Override
    public boolean updateImage(DATA_zone data) {
        boolean success = false;

        SQLiteDatabase db = this.bdSqlite.getWritableDatabase();

        ContentValues editZone = new ContentValues();
        editZone.put("image", data.get_image());

        if (db.update("zone", editZone, "id = " + data.get_id(), null) > 0) {
            String messageToast = this.ctx.getResources().getString(R.string.dataBase_updated).replaceAll("#####", data.get_zone());

            Toast.makeText(this.ctx, messageToast, Toast.LENGTH_SHORT).show();

            success = true;
        }

        db.close();

        return success;
    }

    @Override
    public boolean delete(DATA_zone data) {
        boolean success = false;

        SQLiteDatabase db = this.bdSqlite.getWritableDatabase();

        if (db.delete("zone", "id = " + data.get_id(), null) > 0) {
            String messageToast = this.ctx.getResources().getString(R.string.dataBase_removed).replaceAll("#####", data.get_zone());

            Toast.makeText(this.ctx, messageToast, Toast.LENGTH_SHORT).show();

            success = true;
        }

        db.close();

        return success;
    }

    @Override
    public ArrayList<DATA_zone> read() {
        ArrayList<DATA_zone> data = new ArrayList<>();

        SQLiteDatabase db = this.bdSqlite.getWritableDatabase();

        Cursor cursor = db.rawQuery(DAO_zone_SQL.sql_readAllZone, null);

        if (cursor.moveToFirst()) {
            do {
                DATA_zone zone = new DATA_zone(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2)
                );

//                zone.set_id(cursor.getInt(0));
//                zone.set_zone(cursor.getString(1));
//                zone.set_image(cursor.getString(2), 150, 150, this.ctx);

                data.add(zone);
            } while (cursor.moveToNext());
        }

        db.close();

        return data;
    }

    @Override
    public void setContext(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public boolean valUnique(String sql) {
        boolean unique = false;

        SQLiteDatabase db = this.bdSqlite.getWritableDatabase();

        Cursor row = db.rawQuery(sql, null);

        if (row.moveToFirst())
            unique = true;

        db.close();

        return unique;
    }

    @Override
    public ArrayList<DATA_zone> readWithImages() {
        ArrayList<DATA_zone> data = new ArrayList<>();

        SQLiteDatabase db = this.bdSqlite.getWritableDatabase();

        Cursor cursor = db.rawQuery(DAO_zone_SQL.sql_readAllZone, null);

        if (cursor.moveToFirst()) {
            do {
                DATA_zone zone = new DATA_zone();

                zone.set_id(cursor.getInt(0));
                zone.set_zone(cursor.getString(1));
                zone.set_image(cursor.getString(2));

                data.add(zone);
            } while (cursor.moveToNext());
        }

        db.close();

        return data;
    }
}
