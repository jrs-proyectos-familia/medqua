package medqua.jrs.seyer.hakyn.medqua.activities.zone.tvs;

import android.content.Context;
import android.widget.TextView;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_VARIABLES;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone_ACTI;
import medqua.jrs.seyer.hakyn.medqua.bd.BD_management;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_rules;
import medqua.jrs.seyer.hakyn.medqua.tvs.TVS_treatment;
import medqua.jrs.seyer.hakyn.medqua.tvs.TVS_validation;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 3/03/19 a las 10:02 PM
 * @license Joaquin Reyes Sanchez
 */

public class TVS_zone implements TVS_zone_BASE {
    private DATA_zone dataTreat = null;

    public Object [] fields;
    public FRAME_rules[] rules;
    public TextView [] tvErrors;

    private Context ctx = null;

    private DATA_zone_ACTI argsActivity;

    private TVS_validation validation = new TVS_validation() {
        @Override
        public String getData(String ruleName) {
            String data = "";
            String nameMethod = "get_"+ruleName;

            try {
                for (Method met: TVS_zone.this.dataTreat.getClass().getDeclaredMethods()) {
                    if (met.getName().equals(nameMethod)) {
                        Method method = TVS_zone.this.dataTreat.getClass().getMethod(nameMethod);
                        if (method.getReturnType() == String.class) {
                            data = (String) method.invoke(TVS_zone.this.dataTreat);
                        }
                        break;
                    }
                }

            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

            return data;
        }

        @Override
        public boolean validateUnique(String col, String data) {
            BD_management daoMana = BD_management.init();

            String sql = "SELECT id FROM zone WHERE " + col + " = \"" + data + "\"";

            return daoMana.DAO_Zone(TVS_zone.this.ctx).valUnique(sql);
        }
    };

    public TVS_zone() {
    }

    @Override
    public void treatment() {
        TVS_treatment<DATA_zone> treatment = new TVS_treatment<DATA_zone>() {
            @Override
            public DATA_zone buildingData(String[] treatData) {
                DATA_zone dataZone = new DATA_zone();

                int index = 0;

                for (FRAME_rules rule : TVS_zone.this.rules) {
                    String nameMethod = "set_" + rule.getNameRule();

                    try {
                        for (Method met : dataZone.getClass().getDeclaredMethods()) {
                            if (met.getName().equals(nameMethod)) {
                                Method method = dataZone.getClass().getMethod(nameMethod, String.class);

                                method.invoke(dataZone, treatData[index]);
                                break;
                            }
                        }
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }

                    index++;
                }

                return dataZone;
            }
        };

        this.dataTreat = treatment.treatment(this.fields, this.rules);
    }

    @Override
    public boolean validation() {
        this.treatment();

        return validation.validate(tvErrors, rules, TVS_zone.this.ctx);
    }

    @Override
    public boolean server(Context ctx) {
        this.ctx = ctx;

        if (this.validation()) {
            this.argsActivity = MIX_zone_VARIABLES.getVariablesFromMyActivity(ctx);

            BD_management daoMana = BD_management.init();

            switch (this.argsActivity.getTypeRegister()) {
                case "TVS_NEW":
                    return daoMana.DAO_Zone(ctx).create(this.dataTreat);
                case "TVS_UPDATE":
                    return false;
                case "TVS_UPDATE_ZONE":
                    this.dataTreat.set_id(this.argsActivity.getZoneTemp().get_id());
                    return daoMana.DAO_Zone(ctx).updateZone(this.dataTreat);
                case "TVS_UPDATE_IMAGE":
                    this.dataTreat.set_id(this.argsActivity.getZoneTemp().get_id());
                    this.dataTreat.set_zone(this.argsActivity.getZoneTemp().get_zone());
                    return daoMana.DAO_Zone(ctx).updateImage(this.dataTreat);
            }
        }

        return false;
    }
}
