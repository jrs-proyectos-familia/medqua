package medqua.jrs.seyer.hakyn.medqua.frame;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 12/03/19 a las 07:34 PM
 * @license Joaquin Reyes Sanchez
 */

public interface FRAME_variablesFragment {

    String getTypeRegister ();
    void setTypeRegister (String typeAction);

}
