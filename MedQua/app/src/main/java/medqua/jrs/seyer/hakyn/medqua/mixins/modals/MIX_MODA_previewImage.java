package medqua.jrs.seyer.hakyn.medqua.mixins.modals;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.mixins.photo.MIX_PHOT_fixPhoto;
import medqua.jrs.seyer.hakyn.medqua.mixins.photo.MIX_PHOT_getMemory;

import java.io.File;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 2/03/19 a las 10:04 AM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_MODA_previewImage {

    /**
     *
     * @param ctx Contexto de la DEPRECATED_MIX_ACTI_visorImage
     * @param routeImage Ruta absoluta de la imagen guardada en el dispositivo
     * @return AlertDialog.Builder
     */
    public static AlertDialog.Builder preview (Context ctx, String routeImage) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        LayoutInflater inflater = LayoutInflater.from(ctx);

        final View view = inflater.inflate(R.layout.moda_preview_image, null);
        dialog.setView(view);

        final ImageView imageDialog = view.findViewById(R.id.modal_previewImage_preview);

        File memory = MIX_PHOT_getMemory.sdCard(ctx);
        File photo = new File (memory, routeImage);

        if (photo.exists()) {

            // Para conseguir el ancho y alto del dispositivo
//            DisplayMetrics displayMetrics = new DisplayMetrics();
//            ((Activity)ctx).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//            int width = displayMetrics.widthPixels;
//            int height = displayMetrics.heightPixels;

            Bitmap bitImage = MIX_PHOT_fixPhoto.scalePhoto(photo.getAbsolutePath(), 0, 0);

            imageDialog.setImageBitmap(bitImage);

            Uri uriPhoto = Uri.fromFile(new File(routeImage));
            int rotateImage = MIX_PHOT_fixPhoto.rotateFixPhoto(ctx, uriPhoto);
            imageDialog.setRotation(rotateImage);
        }


        dialog.setNeutralButton(R.string.modal_exit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        return dialog;
    }

}
