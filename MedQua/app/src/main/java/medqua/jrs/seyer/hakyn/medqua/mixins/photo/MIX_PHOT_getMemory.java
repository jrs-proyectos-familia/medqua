package medqua.jrs.seyer.hakyn.medqua.mixins.photo;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import java.io.File;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 8/03/19 a las 10:22 AM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_PHOT_getMemory {

    public static File sdCard (Context ctx) {
        return ContextCompat.getExternalFilesDirs(ctx, null)[1];
    }

}
