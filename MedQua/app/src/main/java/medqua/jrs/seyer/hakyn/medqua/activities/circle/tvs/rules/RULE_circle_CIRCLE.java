package medqua.jrs.seyer.hakyn.medqua.activities.circle.tvs.rules;

import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_rules;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 02:12 PM
 * @license Joaquin Reyes Sanchez
 */

public class RULE_circle_CIRCLE extends FRAME_rules {

    public RULE_circle_CIRCLE() {
        this.nameRule = "circle";
        this.typeRule = String.class;

        this.treatTrim = true;
        this.treatTypeText = "capitalizeFully";
        this.treatSpecialCharacters = true;

        this.valEmpty = true;
        this.valMin = 3;
        this.valMax = 22;
        this.valUnique = true;
    }
}
