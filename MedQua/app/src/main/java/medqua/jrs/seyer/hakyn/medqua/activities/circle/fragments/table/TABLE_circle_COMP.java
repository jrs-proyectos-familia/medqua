package medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.table;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_singleton;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 15/03/19 a las 08:30 AM
 * @license Joaquin Reyes Sanchez
 */

public class TABLE_circle_COMP implements FRAME_singleton {

    private static TABLE_circle_COMP tableCOMP = null;

    public static TABLE_circle_COMP init (Context ctx, View view) {
        if (TABLE_circle_COMP.tableCOMP == null)
            TABLE_circle_COMP.tableCOMP = new TABLE_circle_COMP(ctx, view);

        return TABLE_circle_COMP.tableCOMP;
    }

    private LinearLayout ll_table;

    private TABLE_circle_COMP(Context ctx, View view) {
        this.ll_table = view.findViewById(R.id.list_circles);
    }

    public LinearLayout getLl_table() {
        return ll_table;
    }

    @Override
    public void clearSingleton() {
        TABLE_circle_COMP.tableCOMP = null;
    }
}
