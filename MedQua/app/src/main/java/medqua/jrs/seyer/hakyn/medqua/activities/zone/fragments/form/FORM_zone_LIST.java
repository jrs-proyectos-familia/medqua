package medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.form;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_FIELDS;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_PHOTO;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_REGISTERS;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_RESETS;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_menuFragment;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_rules;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_singleton;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone_ACTI;
import medqua.jrs.seyer.hakyn.medqua.mixins.modals.MIX_MODA_cameraOrGallery;
import medqua.jrs.seyer.hakyn.medqua.mixins.photo.MIX_PHOT_removePhoto;
import medqua.jrs.seyer.hakyn.medqua.mixins.modals.MIX_MODA_previewImage;
import medqua.jrs.seyer.hakyn.medqua.mixins.form.MIX_FORM_checking;
import medqua.jrs.seyer.hakyn.medqua.tvs.TVS_management;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 1/03/19 a las 01:34 PM
 * @license Joaquin Reyes Sanchez
 */

public class FORM_zone_LIST implements FRAME_singleton {

    private static FORM_zone_LIST listZone = null;

    private Context ctx_zone;

    private FORM_zone_COMP compZone;

    private DATA_zone_ACTI argsActivity;
    private FRAME_menuFragment menuActivity;

    public static FORM_zone_LIST init (Context ctx, FORM_zone_COMP compZone) {
        if (FORM_zone_LIST.listZone == null)
            FORM_zone_LIST.listZone = new FORM_zone_LIST(ctx, compZone);

        return FORM_zone_LIST.listZone;
    }

    private FORM_zone_LIST(Context ctx, FORM_zone_COMP compZone) {
        this.ctx_zone = ctx;
        this.compZone = compZone;

        Activity activity = (Activity) FORM_zone_LIST.this.ctx_zone;
        this.argsActivity = ((DATA_zone_ACTI)activity);
        this.menuActivity = (FRAME_menuFragment) activity;

        new Form ();
        new Controls ();
    }

    private String getRouteImage () {
        return (String) FORM_zone_LIST.this.compZone.getIv_image().getTag();
    }

    private class Form {
        private Form () {
            this.buttonImage();
            this.imageDialog();
        }

        private void buttonImage () {
            FORM_zone_LIST
                    .this
                    .compZone
                    .getBtn_image()
                    .setOnClickListener((View v) -> {
                        MIX_zone_PHOTO.removePhoto(FORM_zone_LIST.this.ctx_zone, false);

                        MIX_MODA_cameraOrGallery.selectGalleryOrCam(FORM_zone_LIST.this.ctx_zone, "zone");
                    });
        }

        private void imageDialog () {
            FORM_zone_LIST
                    .this
                    .compZone
                    .getIv_image()
                    .setOnClickListener((View v) -> {
                        if (!getRouteImage().equals("")) {
                            AlertDialog.Builder dialog = MIX_MODA_previewImage.preview(FORM_zone_LIST.this.ctx_zone, getRouteImage());
                            dialog.show();
                        } else Toast.makeText(FORM_zone_LIST.this.ctx_zone, R.string.toast_image_previewNeed, Toast.LENGTH_SHORT).show();
                    });
        }
    }

    private class Controls {
        private Controls () {
            this.newZone();
            this.cleanButton();
            this.sendButton();
        }

        private void newZone () {
            FORM_zone_LIST
                    .this
                    .compZone
                    .getBtnimg_newZone()
                    .setOnClickListener((View v) -> {
                        MIX_zone_PHOTO.removePhoto(FORM_zone_LIST.this.ctx_zone, false);
                        MIX_zone_RESETS.allVariablesFromMyActivity(FORM_zone_LIST.this.ctx_zone);
                        MIX_zone_RESETS.resetForm(FORM_zone_LIST.this.ctx_zone);
                    });
        }

        private void cleanButton () {
            FORM_zone_LIST
                    .this
                    .compZone
                    .getBtnimg_clean()
                    .setOnClickListener((View v) -> {
                        if (FORM_zone_LIST.this.argsActivity.getTypeRegister().equals("TVS_NEW")) {
                            MIX_zone_PHOTO.removePhoto(FORM_zone_LIST.this.ctx_zone, false);
                            MIX_zone_RESETS.allVariablesFromMyActivity(FORM_zone_LIST.this.ctx_zone);
                            MIX_zone_RESETS.resetForm(FORM_zone_LIST.this.ctx_zone);
                        } else {
                            MIX_zone_PHOTO.removePhoto(FORM_zone_LIST.this.ctx_zone, false);
                            MIX_zone_FIELDS.chargeRegister(FORM_zone_LIST.this.ctx_zone);
                            MIX_zone_RESETS.resetErrorsForm(FORM_zone_LIST.this.ctx_zone);
                        }
                    });
        }

        private void sendButton () {
            FORM_zone_LIST
                    .this
                    .compZone
                    .getBtn_send()
                    .setOnClickListener((View v) -> {
                        TVS_management tvsMana = TVS_management.init();

                        Object [] elements = MIX_zone_REGISTERS.fieldsRegister(FORM_zone_LIST.this.ctx_zone, FORM_zone_LIST.this.argsActivity);

                        if (elements != null) {
                            tvsMana.TVS_Zone().fields = (Object[]) elements[0];
                            tvsMana.TVS_Zone().tvErrors = (TextView[]) elements[1];
                            tvsMana.TVS_Zone().rules = (FRAME_rules[]) elements[2];

                            if (MIX_FORM_checking.checkEmptyFields(FORM_zone_LIST.this.ctx_zone, tvsMana.TVS_Zone().fields, tvsMana.TVS_Zone().tvErrors)) {
                                if (tvsMana.TVS_Zone().server(FORM_zone_LIST.this.ctx_zone)) {
                                    if (FORM_zone_LIST.this.argsActivity.getTypeRegister().equals("TVS_UPDATE_IMAGE"))
                                        MIX_PHOT_removePhoto.remove(FORM_zone_LIST.this.argsActivity.getZoneTemp().get_image(), FORM_zone_LIST.this.ctx_zone, false);

                                    MIX_zone_RESETS.allVariablesFromMyActivity(FORM_zone_LIST.this.ctx_zone);
                                    MIX_zone_RESETS.resetForm(FORM_zone_LIST.this.ctx_zone);
                                }
                            }
                        }
                    });
        }
    }

    @Override
    public void clearSingleton() {
        FORM_zone_LIST.listZone = null;
    }
}
