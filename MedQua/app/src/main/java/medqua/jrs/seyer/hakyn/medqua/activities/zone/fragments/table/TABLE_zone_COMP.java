package medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.table;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_singleton;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 5/03/19 a las 09:25 PM
 * @license Joaquin Reyes Sanchez
 */

public class TABLE_zone_COMP implements FRAME_singleton {

    private static TABLE_zone_COMP compZoneTable = null;

    private LinearLayout ll_list;

    public static TABLE_zone_COMP init (Context ctx, View v) {
        if (TABLE_zone_COMP.compZoneTable == null)
            TABLE_zone_COMP.compZoneTable = new TABLE_zone_COMP(ctx, v);

        return TABLE_zone_COMP.compZoneTable;
    }

    private TABLE_zone_COMP(Context ctx, View v) {
        this.ll_list = v.findViewById(R.id.list_zones);
    }

    public LinearLayout getLl_list() {
        return ll_list;
    }

    @Override
    public void clearSingleton() {
        TABLE_zone_COMP.compZoneTable = null;
    }
}
