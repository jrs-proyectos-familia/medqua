package medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.form.FORM_circle_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_FIELDS;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 12/03/19 a las 07:05 PM
 * @license Joaquin Reyes Sanchez
 */

public class FRAG_circle_FORM extends Fragment {
    private Context ctx;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewForm = inflater.inflate(R.layout.circle_form, container, false);

        this.ctx = viewForm.getContext();
        this.view = viewForm;

        FORM_circle_COMP.init(this.ctx, viewForm);

        MIX_circle_FIELDS.blockActionModule(this.ctx);

        MIX_circle_FIELDS.chargeFieldsWithDataTemp(this.ctx);

        return viewForm;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        FORM_circle_COMP.init(null, null).clearSingleton();
    }
}
