package medqua.jrs.seyer.hakyn.medqua.activities.circle.dao;

import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_dao;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 04:16 PM
 * @license Joaquin Reyes Sanchez
 */

public interface DAO_circle_BASE extends FRAME_dao<DATA_circle> {
}
