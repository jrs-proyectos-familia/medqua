package medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.form.FORM_zone_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.tvs.rules.RULE_zone_IMAGE;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.tvs.rules.RULE_zone_ZONE;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_rules;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone_ACTI;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 8/03/19 a las 12:04 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_zone_REGISTERS {

    /**
     *
     * @param argsActivity Indica el acceso a conseguir solo las variables globales de la actividad principal
     * @return Objecto [] [0] -> Contiene los campos (fields) [1] -> Contiene los errores (errors) [2] -> Contiene las reglas (rules)
     */
    public static Object [] fieldsRegister (Context ctx, DATA_zone_ACTI argsActivity) {
        Object [] fields;
        TextView[] errors ;
        FRAME_rules[] rules;

        FORM_zone_COMP compForm = FORM_zone_COMP.init(null, null);

        String routeImage = (String) compForm.getIv_image().getTag();

        fields = new Object[] {
                compForm.getEt_zone(),
                routeImage
        };

        errors = new TextView[] {
                compForm.getTv_error_zone(),
                compForm.getTv_error_imageZone()
        };

        rules = new FRAME_rules[] {
                new RULE_zone_ZONE(),
                new RULE_zone_IMAGE()
        };

        if (argsActivity.getTypeRegister().equals("TVS_NEW")) {
            // Nuevo Registro
            return new Object[] {
                fields,
                errors,
                rules
            };
        } else {
            // Actualización de Registro
            DATA_zone newData = new DATA_zone();
            newData.set_zone(compForm.getEt_zone().getText().toString());
            newData.set_image((String) compForm.getIv_image().getTag());

            boolean valZone = newData.get_zone().equals(argsActivity.getZoneTemp().get_zone());
            boolean valImage = newData.get_image().equals(argsActivity.getZoneTemp().get_image());

            if (valZone && valImage) {
                Toast.makeText(ctx, ctx.getResources().getString(R.string.toast_dataTemp_noChanges), Toast.LENGTH_SHORT).show();

                return null;
            } else {
                switch (argsActivity.getTypeRegister()) {
                    case "TVS_UPDATE":
                        return new Object[] {
                            fields,
                            errors,
                            rules
                        };
                    case "TVS_UPDATE_ZONE":
                        return new Object[] {
                            new Object[] {
                                fields[0]
                            },
                            new TextView[] {
                                errors[0]
                            },
                            new FRAME_rules[] {
                                rules[0]
                            }
                        };

                    case "TVS_UPDATE_IMAGE":
                        return new Object[] {
                            new Object[] {
                                fields[1]
                            },
                            new TextView[] {
                                errors[1]
                            },
                            new FRAME_rules[] {
                                rules[1]
                            }
                        };
                    default:
                        return null;
                }
            }
        }
    }

}
