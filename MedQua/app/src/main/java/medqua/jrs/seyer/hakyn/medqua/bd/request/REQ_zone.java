package medqua.jrs.seyer.hakyn.medqua.bd.request;

import android.content.Context;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone;
import medqua.jrs.seyer.hakyn.medqua.bd.BD_management;

import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 5/03/19 a las 09:51 PM
 * @license Joaquin Reyes Sanchez
 */

public class REQ_zone {

    public static ArrayList<DATA_zone> getAllZones (Context ctx) {
        BD_management daoMana = BD_management.init();

        return daoMana.DAO_Zone(ctx).read();
    }

    public static ArrayList<DATA_zone> getAllZonesWithImages (Context ctx) {
        BD_management daoMana = BD_management.init();

        return daoMana.DAO_Zone(ctx).readWithImages();
    }

}
