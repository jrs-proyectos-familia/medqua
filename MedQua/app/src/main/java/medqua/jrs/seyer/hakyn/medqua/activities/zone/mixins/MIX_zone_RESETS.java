package medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins;

import android.content.Context;
import android.widget.TextView;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.form.FORM_zone_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.table.TABLE_zone_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone_ACTI;
import medqua.jrs.seyer.hakyn.medqua.mixins.form.MIX_FORM_resetFields;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 8/03/19 a las 10:42 AM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_zone_RESETS extends MIX_FORM_resetFields {
    public static void resetErrorsForm (Context ctx) {
        FORM_zone_COMP compZoneForm = FORM_zone_COMP.init(null, null);

        TextView [] errors = new TextView[] {
                compZoneForm.getTv_error_zone(),
                compZoneForm.getTv_error_imageZone()
        };

        MIX_FORM_resetFields.resetFields(errors);
    }

    /**
     *
     * @param ctx Indica el Contexto de de la Activity
     */
    public static void resetForm (Context ctx) {
        FORM_zone_COMP compZoneForm = FORM_zone_COMP.init(null, null);

        Object [] fields = new Object[] {
            compZoneForm.getEt_zone(),
            compZoneForm.getIv_image()
        };

        MIX_FORM_resetFields.resetFields(fields);

        MIX_zone_RESETS.resetErrorsForm(ctx);
        MIX_zone_FIELDS.blockActionZone(ctx);
    }

    /**
     *
     * @param ctx Indica el context de la actividad principal
     */
    public static void allVariablesFromMyActivity (Context ctx) {
        DATA_zone_ACTI varsZone = MIX_zone_VARIABLES.getVariablesFromMyActivity(ctx);

        varsZone.setZoneTemp(null);
        varsZone.setTypeRegister("TVS_NEW");

        varsZone.setStateThreadTableImages(false);
    }

    public static void resetTable () {
        TABLE_zone_COMP compZoneTable = TABLE_zone_COMP.init(null, null);

        compZoneTable.getLl_list().removeAllViews();
    }

}
