package medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.form.FORM_zone_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.panel.PANEL_zone_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone_ACTI;
import medqua.jrs.seyer.hakyn.medqua.mixins.photo.MIX_PHOT_fixPhoto;
import medqua.jrs.seyer.hakyn.medqua.mixins.photo.MIX_PHOT_getMemory;

import java.io.File;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 8/03/19 a las 01:44 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_zone_FIELDS {

    public static void blockActionPanel (Context ctx) {
        DATA_zone_ACTI argsActivity = MIX_zone_VARIABLES.getVariablesFromMyActivity(ctx);

        // DEL PANEL
        PANEL_zone_COMP compZonePanel = PANEL_zone_COMP.init(null, null);

        String btnNewZone_text = ctx.getResources().getString(R.string.zone_panel_newRegister);

        switch (argsActivity.getTypeRegister()) {
            case "TVS_UPDATE_ZONE":
                //  DEL PANEL
                btnNewZone_text = ctx.getResources().getString(R.string.zone_panel_editRegister);
                break;
            case "TVS_UPDATE_IMAGE":
                //  DEL PANEL
                btnNewZone_text = ctx.getResources().getString(R.string.zone_panel_editRegister);
                break;
        }

        // DEL PANEL
        compZonePanel.getBtn_newZone().setText(btnNewZone_text);
    }

    public static void blockActionZone (Context ctx) {
        DATA_zone_ACTI argsActivity = MIX_zone_VARIABLES.getVariablesFromMyActivity(ctx);

        MIX_zone_FIELDS.blockActionPanel(ctx);

        // DEL FORMULARIO
        FORM_zone_COMP compZoneForm = FORM_zone_COMP.init(null, null);

        boolean formZone_enable = true;
        int formZone_color = ctx.getResources().getColor(R.color.main_h);
        Drawable formZone_style = ctx.getResources().getDrawable(R.drawable.style_inputs);

        boolean formImage_enable = true;
        int formImage_color = ctx.getResources().getColor(R.color.main_h);
        Drawable formImage_style = ctx.getResources().getDrawable(R.drawable.style_form_buttons);

        String btnSend_text = ctx.getResources().getString(R.string.action_send);
        int btnSend_color = ctx.getResources().getColor(R.color.main_h);

        int btnNewReg_visible = View.INVISIBLE;

        switch (argsActivity.getTypeRegister()) {
            case "TVS_UPDATE_ZONE":
                // DEL FORMULARIO
                btnSend_text = ctx.getResources().getString(R.string.action_edit);
                btnSend_color = ctx.getResources().getColor(R.color.secondary_yellow);

                formImage_enable = false;
                formImage_color = ctx.getResources().getColor(R.color.support_white);
                formImage_style = ctx.getResources().getDrawable(R.drawable.style_form_buttons_disabled);

                btnNewReg_visible = View.VISIBLE;
                break;
            case "TVS_UPDATE_IMAGE":
                // DEL FORMULARIO
                btnSend_text = ctx.getResources().getString(R.string.action_edit);
                btnSend_color = ctx.getResources().getColor(R.color.secondary_yellow);

                formZone_enable = false;
                formZone_color = ctx.getResources().getColor(R.color.support_white);
                formZone_style = ctx.getResources().getDrawable(R.drawable.style_inputs_disabled);

                btnNewReg_visible = View.VISIBLE;
                break;
        }

        // DEL FORMULARIO
        int paddingInputs = (int) ctx.getResources().getDimension(R.dimen.padding_df);

        compZoneForm.getBtn_send().setText(btnSend_text);
        compZoneForm.getBtn_send().setBackgroundColor(btnSend_color);

        compZoneForm.getEt_zone().setEnabled(formZone_enable);
        compZoneForm.getEt_zone().setTextColor(formZone_color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            compZoneForm.getEt_zone().setBackground(formZone_style);
        compZoneForm.getEt_zone().setPadding(paddingInputs, paddingInputs, paddingInputs, paddingInputs);

        compZoneForm.getBtn_image().setEnabled(formImage_enable);
        compZoneForm.getBtn_image().setTextColor(formImage_color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            compZoneForm.getBtn_image().setBackground(formImage_style);

        compZoneForm.getBtnimg_newZone().setVisibility(btnNewReg_visible);

    }

    public static void chargeRegister (Context ctx) {
        DATA_zone_ACTI argsActivity = MIX_zone_VARIABLES.getVariablesFromMyActivity(ctx);
        FORM_zone_COMP compZoneForm = FORM_zone_COMP.init(null, null);

        if (argsActivity.getZoneTemp() != null) {
            compZoneForm.getEt_zone().setText(argsActivity.getZoneTemp().get_zone());

            File memory = MIX_PHOT_getMemory.sdCard(ctx);
            File fileImage = new File(memory, argsActivity.getZoneTemp().get_image());
            if (fileImage.exists()) {
                Bitmap imageBit = MIX_PHOT_fixPhoto.scalePhoto(fileImage.getAbsolutePath(), 100, 100);
                compZoneForm.getIv_image().setImageBitmap(imageBit);
                compZoneForm.getIv_image().setTag(argsActivity.getZoneTemp().get_image());
            }
        }
    }

}
