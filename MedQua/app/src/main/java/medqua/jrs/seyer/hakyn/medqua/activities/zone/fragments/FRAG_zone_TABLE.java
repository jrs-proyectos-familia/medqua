package medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.table.TABLE_zone_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_TABLE;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone_ACTI;

import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 5/03/19 a las 07:48 PM
 * @license Joaquin Reyes Sanchez
 */

public class FRAG_zone_TABLE extends Fragment {

    private Context ctx;

    private ArrayList<DATA_zone> allZones = null;

    private DATA_zone_ACTI argsActivity;

    public FRAG_zone_TABLE() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.zone_list_zones, container, false);

        this.ctx = view.getContext();
        this.argsActivity = (DATA_zone_ACTI)this.getActivity();

        TABLE_zone_COMP.init(this.ctx, view);

        this.allZones = MIX_zone_TABLE.chargeTable(this.ctx);

// FORMA VIEJA USANDO UN LISTVIEW (Lo dejé puesto que la forma nueva maneja mucho mejor la renderización de Imagenes)
//        TABLE_zone_COMP compZoneTable = TABLE_zone_COMP.init(this.ctx, view);
//
//        this.allZones = REQ_zone.getAllZones(view.getContext());
//
//        compZoneTable.getLv_zone().setAdapter(new MIX_ADAP_list_default<DATA_zone>(view.getContext(), this.allZones) {
//            @Override
//            public View getView(int position, View convertView, ViewGroup parent) {
//                final View view = inflater.inflate(R.layout.list_image_basic_a, null);
//
//                TABLE_ROW_zone_COMP comp = new TABLE_ROW_zone_COMP(view.getContext(), view);
//
//                comp.getTl_zoneTitle().setText(this.getData().get(position).get_zone());
//                comp.getTl_zoneId().setText(this.getData().get(position).get_id().toString());
//                if (this.getData().get(position).getImageBit() != null)
//                    comp.getIv_zoneImage().setImageBitmap(this.getData().get(position).getImageBit());
//
////                File sdCard = ContextCompat.getExternalFilesDirs(FRAG_zone_TABLE.this.ctx, null)[1];
////                File photo = new File(sdCard, this.getData().get(position).get_image());
////                Bitmap imageBit = MIX_PHOT_fixPhoto.scalePhoto(photo.getAbsolutePath(), 0, 0);
////                comp.getIv_zoneImage().setTag(imageBit);
//
//                comp.getBtnimg_edit().setTag(this.getData().get(position));
//                comp.getBtnimg_delete().setTag(this.getData().get(position));
//
//                return comp.getView();
//            }
//        });

        return view;
    }

    @Override
    public void onResume () {
        super.onResume();

        MIX_zone_TABLE.chargeImagesTable(this.ctx, this.allZones);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        this.argsActivity.setStateThreadTableImages(true);

        TABLE_zone_COMP.init(null, null).clearSingleton();
    }
}
