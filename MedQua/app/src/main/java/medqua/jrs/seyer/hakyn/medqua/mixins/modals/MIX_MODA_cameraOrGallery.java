package medqua.jrs.seyer.hakyn.medqua.mixins.modals;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.mixins.photo.MIX_PHOT_takePhoto;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 4/03/19 a las 03:24 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_MODA_cameraOrGallery {

    public static void selectGalleryOrCam (Context ctx, String category) {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(ctx);
        View view = View.inflate(ctx, R.layout.moda_chose_camera_gallery, null);

        ImageView iv_cam = view.findViewById(R.id.modal_cameraOrGallery_camera);
        ImageView iv_gal = view.findViewById(R.id.modal_cameraOrGallery_gallery);

        pictureDialog.setView(view);

        AlertDialog showDialog = pictureDialog.show();

        iv_gal.setOnClickListener((View v) -> {
            openGallery(ctx);
            showDialog.dismiss();
        });

        iv_cam.setOnClickListener((View v) -> {
            MIX_PHOT_takePhoto.photo(ctx, category);
            showDialog.dismiss();
        });

    }

    public static void openGallery (Context ctx) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        galleryIntent.setType("image/*");

        ((Activity)ctx).startActivityForResult(galleryIntent, 2);
    }

}
