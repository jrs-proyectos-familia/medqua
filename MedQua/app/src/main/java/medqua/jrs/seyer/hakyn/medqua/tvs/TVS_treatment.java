package medqua.jrs.seyer.hakyn.medqua.tvs;

import android.widget.EditText;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_rules;
import medqua.jrs.seyer.hakyn.medqua.tools.TOOL_treatment;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 2/03/19 a las 09:40 PM
 * @license Joaquin Reyes Sanchez
 */

public abstract class TVS_treatment<T>{

    /**
     *
     * @param fields Indica el conjunto de componentes a tratar
     * @param rules Indica las reglas que deberán seguir todos los componentes (trim, type of text, accentes, ...)
     * @return String [] un img_nuevo arreglo con los nuevos datos tratados
     */
    public T treatment (Object [] fields, FRAME_rules[] rules) {
        int index = 0;
        String [] newData = new String [fields.length];

        for (Object field : fields) {
            String data = "";

            if (field instanceof EditText)
                data = ((EditText)field).getText().toString();
            else if (field instanceof String)
                data = (String)field;

            if (!data.equals("")) {
                if (rules[index].isTreatSpecialCharacters() == true && rules[index].isTreatTrim() == true)
                    newData[index] = TOOL_treatment.trimTextAccents(data, rules[index].getTreatTypeText());
                else {
                    if (rules[index].isTreatTrim())
                        newData[index] = TOOL_treatment.trim(data);
                    if (!rules[index].getTreatTypeText().equals("none"))
                        newData[index] = TOOL_treatment.text(data, rules[index].getTreatTypeText());
                    if (rules[index].isTreatSpecialCharacters())
                        newData[index] = TOOL_treatment.accents(data);
                }
            }

            index++;
        }

        return this.buildingData(newData);
    }

    /**
     *
     * @param treatData Indica el arreglo de los datos ya tratados
     * @return T Objecto contenedora de los datos ya tratados
     */
    public abstract T buildingData (String [] treatData);

}
