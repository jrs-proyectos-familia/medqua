package medqua.jrs.seyer.hakyn.medqua.mixins.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import medqua.jrs.seyer.hakyn.medqua.R;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 6/03/19 a las 03:07 PM
 * @license Joaquin Reyes Sanchez
 */

public class DEPRECATED_MIX_ACTI_visorImage extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.deprecated_acti_visor_image);

        ImageView image = findViewById(R.id.vi_image);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();

        if (b != null)
            image.setImageBitmap((Bitmap) b.get("IMAGE"));
    }
}
