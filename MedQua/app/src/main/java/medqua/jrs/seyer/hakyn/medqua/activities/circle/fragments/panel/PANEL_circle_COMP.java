package medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.panel;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_singleton;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 01:36 PM
 * @license Joaquin Reyes Sanchez
 */

public class PANEL_circle_COMP implements FRAME_singleton {

    private static PANEL_circle_COMP panelCOMP = null;

    public static PANEL_circle_COMP init (Context ctx, View view) {
        if (PANEL_circle_COMP.panelCOMP == null)
            PANEL_circle_COMP.panelCOMP = new PANEL_circle_COMP(ctx, view);

        return PANEL_circle_COMP.panelCOMP;
    }

    private Button btn_newCircle, btn_tabCircle;

    private PANEL_circle_COMP(Context ctx, View view) {
        this.btn_newCircle = view.findViewById(R.id.panel_newRegister);
        this.btn_tabCircle = view.findViewById(R.id.panel_list);

        PANEL_circle_LIST.init(ctx, this);
    }

    public Button getBtn_newCircle() {
        return btn_newCircle;
    }

    public Button getBtn_tabCircle() {
        return btn_tabCircle;
    }

    @Override
    public void clearSingleton() {
        PANEL_circle_COMP.panelCOMP = null;
        PANEL_circle_LIST.init(null, null).clearSingleton();
    }
}
