package medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.table;

import android.content.Context;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import medqua.jrs.seyer.hakyn.medqua.R;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 15/03/19 a las 08:44 AM
 * @license Joaquin Reyes Sanchez
 */

public class TABLE_ROW_circle_COMP {
    private TextView tv_title;
    private ImageButton btnimg_edit, btnimg_delete;

    public TABLE_ROW_circle_COMP(Context ctx, View view) {
        this.tv_title = view.findViewById(R.id.list_basic_A_title);
        this.btnimg_edit = view.findViewById(R.id.list_basic_A_button_A);
        this.btnimg_delete = view.findViewById(R.id.list_basic_A_button_B);

        new TABLE_ROW_circle_LIST(ctx, this);
    }

    public TextView getTv_title() {
        return tv_title;
    }

    public ImageButton getBtnimg_edit() {
        return btnimg_edit;
    }

    public ImageButton getBtnimg_delete() {
        return btnimg_delete;
    }
}
