package medqua.jrs.seyer.hakyn.medqua.frame;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 8/03/19 a las 05:12 PM
 * @license Joaquin Reyes Sanchez
 */

public class FRAME_rules {

    protected String nameRule;
    protected Class typeRule;

    protected boolean treatTrim;
    protected String treatTypeText;
    protected boolean treatSpecialCharacters;

    protected boolean valEmpty;
    protected int valMin;
    protected int valMax;
    protected boolean valUnique;

    public Class getTypeRule() {
        return typeRule;
    }

    public String getNameRule() {
        return nameRule;
    }

    public boolean isTreatTrim() {
        return treatTrim;
    }

    public String getTreatTypeText() {
        return treatTypeText;
    }

    public boolean isTreatSpecialCharacters() {
        return treatSpecialCharacters;
    }

    public boolean isValEmpty() {
        return valEmpty;
    }

    public int getValMin() {
        return valMin;
    }

    public int getValMax() {
        return valMax;
    }

    public boolean isValUnique() {
        return valUnique;
    }
}
