package medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.form;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_FIELDS;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_REGISTERS;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_RESETS;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_VARIABLES;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle_ACTI;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_rules;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_singleton;
import medqua.jrs.seyer.hakyn.medqua.mixins.form.MIX_FORM_checking;
import medqua.jrs.seyer.hakyn.medqua.tvs.TVS_management;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 12/03/19 a las 07:19 PM
 * @license Joaquin Reyes Sanchez
 */

public class FORM_circle_LIST implements FRAME_singleton {
    private static FORM_circle_LIST listCircle = null;

    public static FORM_circle_LIST init (Context ctx, FORM_circle_COMP compCircle) {
        if (FORM_circle_LIST.listCircle == null)
            FORM_circle_LIST.listCircle = new FORM_circle_LIST(ctx, compCircle);

        return FORM_circle_LIST.listCircle;
    }

    private Context ctx;

    private FORM_circle_COMP circleCOMP;
    private DATA_circle_ACTI circleVARS;

    private FORM_circle_LIST(Context ctx, FORM_circle_COMP compCircle) {
        this.ctx = ctx;
        this.circleCOMP = compCircle;
        this.circleVARS = MIX_circle_VARIABLES.getVariablesFromMyActivity(this.ctx);

        new Controls();
    }

    private class Controls {
        private Controls () {
            this.newCircle();
            this.clean();
            this.send();
        }

        private void newCircle () {
            FORM_circle_LIST
                    .this
                    .circleCOMP
                    .getBtnImg_newCircle()
                    .setOnClickListener((View v) -> {
                        MIX_circle_RESETS.resetAllVariablesFromMyActivity(FORM_circle_LIST.this.ctx);
                        MIX_circle_RESETS.resetForm(FORM_circle_LIST.this.ctx);
                    });
        }

        private void clean () {
            FORM_circle_LIST
                    .this
                    .circleCOMP
                    .getBtnImg_clean()
                    .setOnClickListener((View v) -> {
                        if (FORM_circle_LIST.this.circleVARS.getTypeRegister().equals("TVS_NEW")) {
                            MIX_circle_RESETS.resetAllVariablesFromMyActivity(FORM_circle_LIST.this.ctx);
                            MIX_circle_RESETS.resetForm(FORM_circle_LIST.this.ctx);
                        } else {
                            MIX_circle_FIELDS.chargeFieldsWithDataTemp(FORM_circle_LIST.this.ctx);
                            MIX_circle_RESETS.resetErrors(FORM_circle_LIST.this.ctx);
                        }
                    });
        }

        private void send () {
            FORM_circle_LIST
                    .this
                    .circleCOMP
                    .getBtn_send()
                    .setOnClickListener((View v) -> {
                        TVS_management manaTVS = TVS_management.init();

                        Object [] elements = MIX_circle_REGISTERS.dataTVS(FORM_circle_LIST.this.ctx);

                        if (elements != null) {
                            manaTVS.TVS_circle().fields = (Object[]) elements[0];
                            manaTVS.TVS_circle().errors = (TextView[]) elements[1];
                            manaTVS.TVS_circle().rules = (FRAME_rules[]) elements[2];

                            if (MIX_FORM_checking.checkEmptyFields(FORM_circle_LIST.this.ctx, manaTVS.TVS_circle().fields, manaTVS.TVS_circle().errors)){
                                if (manaTVS.TVS_circle().server(FORM_circle_LIST.this.ctx)) {
                                    MIX_circle_RESETS.resetAllVariablesFromMyActivity(FORM_circle_LIST.this.ctx);
                                    MIX_circle_RESETS.resetForm(FORM_circle_LIST.this.ctx);
                                }
                            }
                        }
                    });
        }
    }

    @Override
    public void clearSingleton() {
        FORM_circle_LIST.listCircle = null;
    }
}
