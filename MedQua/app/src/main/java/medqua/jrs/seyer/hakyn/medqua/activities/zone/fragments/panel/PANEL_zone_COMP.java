package medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.panel;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_singleton;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 5/03/19 a las 10:13 AM
 * @license Joaquin Reyes Sanchez
 */

public class PANEL_zone_COMP implements FRAME_singleton {

    private static PANEL_zone_COMP compZonePanel = null;

    private Button btn_newZone, btn_tabZone;

    public static PANEL_zone_COMP init (Context ctx, View v) {
        if (PANEL_zone_COMP.compZonePanel == null)
            PANEL_zone_COMP.compZonePanel = new PANEL_zone_COMP(ctx, v);

        return PANEL_zone_COMP.compZonePanel;
    }

    private PANEL_zone_COMP(Context ctx, View v) {
        this.btn_newZone = v.findViewById(R.id.panel_newRegister);
        this.btn_tabZone = v.findViewById(R.id.panel_list);

        PANEL_zone_LIST.init(ctx, this);
    }

    @Override
    public void clearSingleton() {
        PANEL_zone_COMP.compZonePanel = null;
        PANEL_zone_LIST.init(null, null).clearSingleton();
    }

    public Button getBtn_newZone() {
        return btn_newZone;
    }

    public Button getBtn_tabZone() {
        return btn_tabZone;
    }
}
