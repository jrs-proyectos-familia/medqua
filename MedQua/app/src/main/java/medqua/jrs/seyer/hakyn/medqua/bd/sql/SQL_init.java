package medqua.jrs.seyer.hakyn.medqua.bd.sql;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/02/19 a las 09:44 PM
 * @license Joaquin Reyes Sanchez
 */

public interface SQL_init {

    String TABLE_ZONE = "CREATE TABLE zone (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            "zone TEXT NOT NULL UNIQUE," +
            "image TEXT UNIQUE" +
            ")";

    String TABLE_CIRCLE = "CREATE TABLE circle (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            "circle TEXT UNIQUE" +
            ")";

    String TABLE_WATERMETER = "CREATE TABLE watermeter (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            "number TEXT NOT NULL UNIQUE," +
            "image TEXT NOT NULL UNIQUE," +
            "longitude TEXT NOT NULL UNIQUE," +
            "latitude TEXT NOT NULL UNIQUE," +
            "notes TEXT" +
            ")";

    String TABLE_USER = "CREATE TABLE user (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            "name TEXT NOT NULL," +
            "surnames TEXT NOT NULL," +
            "address TEXT," +
            "zone INTEGER NOT NULL," +
            "circle INTEGER NOT NULL," +
            "watermeter INT," +
            "image TEXT UNIQUE," +
            "line INT UNIQUE DEFAULT -1," +
            "created NUMERIC NOT NULL," +
            "updated NUMERIC NOT NULL," +
            "notes TEXT," +
            "FOREIGN KEY (zone) " +
            "   REFERENCES zone(id) " +
            "       ON UPDATE CASCADE " +
            "       ON DELETE RESTRICT," +
            "FOREIGN KEY (circle) " +
            "   REFERENCES circle(id) " +
            "       ON UPDATE CASCADE " +
            "       ON DELETE RESTRICT," +
            "FOREIGN KEY (watermeter)  " +
            "   REFERENCES watermeter(id) " +
            "       ON UPDATE CASCADE " +
            "       ON DELETE RESTRICT "+
            ")";

    String TABLE_PAY = "CREATE TABLE pay (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            "consumed INTEGER NOT NULL," +
            "rate TEXT NOT NULL," +
            "pay REAL NOT NULL," +
            "money_received REAL NOT NULL," +
            "money_returned REAL NOT NULL," +
            "created NUMERIC NOT NULL," +
            "updated NUMERIC NOT NULL," +
            "changes TEXT," +
            "notes TEXT" +
            ")";

    String TABLE_READING = "CREATE TABLE reading (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            "user INTEGER NOT NULL," +
            "worker INTEGER NOT NULL," +
            "reading INTEGER NOT NULL," +
            "created NUMERIC NOT NULL," +
            "updated NUMERIC NOT NULL," +
            "changes TEXT," +
            "notes TEXT," +
            "image TEXT," +
            "pay INTEGER," +
            "FOREIGN KEY (user) " +
            "   REFERENCES user(id) " +
            "       ON UPDATE CASCADE " +
            "       ON DELETE RESTRICT, " +
            "FOREIGN KEY (worker) " +
            "   REFERENCES user(id) " +
            "       ON UPDATE CASCADE " +
            "       ON DELETE RESTRICT," +
            "FOREIGN KEY (pay) " +
            "   REFERENCES pay(id) " +
            "       ON UPDATE CASCADE " +
            "       ON DELETE RESTRICT" +
            ")";

    String TABLE_RATE = "CREATE TABLE rate (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            "minimum INT NOT NULL UNIQUE," +
            "maximum INT NOT NULL UNIQUE," +
            "rate DOUBLE NOT NULL," +
            "fixed_increased INTEGER NOT NULL" +
            ")";
}
