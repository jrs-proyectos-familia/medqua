package medqua.jrs.seyer.hakyn.medqua.bd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import medqua.jrs.seyer.hakyn.medqua.bd.sql.SQL_init;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 28/02/19 a las 09:42 PM
 * @license Joaquin Reyes Sanchez
 */

public class BD_sqlite extends SQLiteOpenHelper {

    /**
     *
     * @param context Indica el contexto de la DEPRECATED_MIX_ACTI_visorImage
     * @param dataBase Indica el nombre de la base de datos a usar
     * @param factory
     * @param version
     */
    public BD_sqlite(Context context, String dataBase, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, dataBase, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_init.TABLE_ZONE);
        db.execSQL(SQL_init.TABLE_CIRCLE);
        db.execSQL(SQL_init.TABLE_WATERMETER);
        db.execSQL(SQL_init.TABLE_USER);
        db.execSQL(SQL_init.TABLE_PAY);
        db.execSQL(SQL_init.TABLE_READING);
        db.execSQL(SQL_init.TABLE_RATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
