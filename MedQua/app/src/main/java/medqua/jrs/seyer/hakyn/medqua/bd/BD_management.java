package medqua.jrs.seyer.hakyn.medqua.bd;

import android.content.Context;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.dao.DAO_circle;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.dao.DAO_zone;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_singleton;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 4/03/19 a las 09:55 AM
 * @license Joaquin Reyes Sanchez
 */

public class BD_management implements FRAME_singleton {

    private static BD_management bdManagement = null;

    private DAO_zone daoZone = null;
    private DAO_circle daoCircle = null;

    public static BD_management init () {
        if (BD_management.bdManagement == null)
            BD_management.bdManagement = new BD_management();

        return BD_management.bdManagement;
    }

    private BD_management() {}

    public DAO_zone DAO_Zone (Context ctx) {
        if (this.daoZone == null)
            this.daoZone = new DAO_zone(ctx);
        else this.daoZone.setContext(ctx);

        return this.daoZone;
    }

    public  DAO_circle DAO_Circle (Context ctx) {
        if (this.daoCircle == null)
            this.daoCircle = new DAO_circle(ctx);
        else this.daoCircle.setContext(ctx);

        return this.daoCircle;
    }

    @Override
    public void clearSingleton() {
        BD_management.bdManagement = null;
    }
}
