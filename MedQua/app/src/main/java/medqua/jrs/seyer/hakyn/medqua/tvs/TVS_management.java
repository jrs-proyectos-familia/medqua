package medqua.jrs.seyer.hakyn.medqua.tvs;

import medqua.jrs.seyer.hakyn.medqua.activities.circle.tvs.TVS_circle;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.tvs.TVS_zone;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_singleton;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 4/03/19 a las 12:22 PM
 * @license Joaquin Reyes Sanchez
 */

public class TVS_management implements FRAME_singleton {

    private static TVS_management tvsManagement = null;

    private TVS_zone tvsZone = null;
    private TVS_circle tvsCircle = null;

    public static TVS_management init () {
        if (TVS_management.tvsManagement == null)
            TVS_management.tvsManagement = new TVS_management();

        return TVS_management.tvsManagement;
    }

    private TVS_management() {}

    public TVS_zone TVS_Zone () {
        if (this.tvsZone == null)
            this.tvsZone = new TVS_zone();

        return this.tvsZone;
    }

    public TVS_circle TVS_circle () {
        if (this.tvsCircle == null)
            this.tvsCircle = new TVS_circle();

        return this.tvsCircle;
    }

    @Override
    public void clearSingleton() {
        TVS_management.tvsManagement = null;
    }
}
