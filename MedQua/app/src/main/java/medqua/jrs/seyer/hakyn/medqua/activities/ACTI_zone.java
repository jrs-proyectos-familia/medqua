package medqua.jrs.seyer.hakyn.medqua.activities;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Toast;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.FRAG_zone_FORM;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.FRAG_zone_TABLE;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.form.FORM_zone_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.panel.PANEL_zone_COMP;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_menuFragment;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone_ACTI;
import medqua.jrs.seyer.hakyn.medqua.mixins.photo.MIX_PHOT_fixPhoto;
import medqua.jrs.seyer.hakyn.medqua.mixins.photo.MIX_PHOT_getMemory;
import medqua.jrs.seyer.hakyn.medqua.mixins.photo.MIX_PHOT_takePhoto;

import java.io.File;
import java.io.IOException;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 1/03/19 a las 09:55 AM
 * @license Joaquin Reyes Sanchez
 */

public class ACTI_zone extends Activity implements FRAME_menuFragment, DATA_zone_ACTI {

    private String typeRegister = "TVS_NEW";
    private DATA_zone zoneTemp = null;
    private boolean stateThreadTableImages = false;

    @Override
    public void onCreate (Bundle instance) {
        super.onCreate(instance);

        this.setContentView(R.layout.zone);

        if (Build.VERSION.SDK_INT >= 23) {
            String thePermissions [] = {
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
            };

            requestPermissions(thePermissions, 2);
        }

        this.menuFragment("MENU_FORM");
    }

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        File memory = MIX_PHOT_getMemory.sdCard(this);
        FORM_zone_COMP compZone = FORM_zone_COMP.init(null,null);

        boolean successImage = true;

        if (requestCode == 1) {
            // Proviene de la camara
            File filePhoto = new File(memory, (String) compZone.getIv_image().getTag());

            if (filePhoto.exists()) {
                Toast.makeText(this, R.string.toast_image_save, Toast.LENGTH_SHORT).show();

                Bitmap imageBitmap = MIX_PHOT_fixPhoto.scalePhoto(filePhoto.getAbsolutePath(), 100, 100);

                Uri uriPhoto = Uri.fromFile(filePhoto);
                int rotateImage = MIX_PHOT_fixPhoto.rotateFixPhoto(this, uriPhoto);

                compZone.getIv_image().setImageBitmap(imageBitmap);
                compZone.getIv_image().setRotation(rotateImage);
            } else successImage = false;

        } else if (requestCode == 2) {
            // Proviene de la galeria
            if (data != null) {
                Uri uriImage = data.getData();

                try {
                    Bitmap imageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uriImage);

                    String routeImage = MIX_PHOT_takePhoto.saveImageFromGallery(this, "zone", imageBitmap);

                    File filePhoto = new File (memory, routeImage);

                    Uri uriPhoto = Uri.fromFile(filePhoto);

                    if (filePhoto.exists()) {
                        int rotateImage = MIX_PHOT_fixPhoto.rotateFixPhoto(this, uriPhoto);

                        Bitmap imageScaled = MIX_PHOT_fixPhoto.scalePhoto(imageBitmap, 100, 100);

                        compZone.getIv_image().setImageBitmap(imageScaled);
                        compZone.getIv_image().setRotation(rotateImage);
                        compZone.getIv_image().setTag(routeImage);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else successImage = false;
        }

        if (!successImage) {
            if (this.zoneTemp == null) {
                compZone.getIv_image().setImageBitmap(null);
                compZone.getIv_image().setTag("");
            } else {
                File fileImage = new File(memory, this.zoneTemp.get_image());
                if (fileImage.exists()) {
                    Bitmap imageBit = MIX_PHOT_fixPhoto.scalePhoto(fileImage.getAbsolutePath(), 100, 100);
                    compZone.getIv_image().setImageBitmap(imageBit);
                    compZone.getIv_image().setTag(this.zoneTemp.get_image());
                }
            }
        }
    }

    @Override
    public void menuFragment(String fragment) {
        FragmentManager fm = this.getFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();

        PANEL_zone_COMP compZonePanel = PANEL_zone_COMP.init(null, null);

        switch (fragment) {
            case "MENU_FORM":
                ft.replace(R.id.zone_content, new FRAG_zone_FORM());

                compZonePanel.getBtn_newZone().setBackgroundColor(getResources().getColor(R.color.main_s));
                compZonePanel.getBtn_tabZone().setBackgroundColor(getResources().getColor(R.color.main_h));
                break;
            case "MENU_TABLE":
                ft.replace(R.id.zone_content, new FRAG_zone_TABLE());

                compZonePanel.getBtn_newZone().setBackgroundColor(getResources().getColor(R.color.main_h));
                compZonePanel.getBtn_tabZone().setBackgroundColor(getResources().getColor(R.color.main_s));
                break;
        }

        ft.commit();
    }

    @Override
    public DATA_zone getZoneTemp() {
        return this.zoneTemp;
    }

    @Override
    public void setZoneTemp(DATA_zone zone) {
        this.zoneTemp = zone;
    }

    @Override
    public String getTypeRegister() {
        return this.typeRegister;
    }

    @Override
    public void setTypeRegister(String typeEdit) {
        this.typeRegister = typeEdit;
    }

    @Override
    public boolean getStateThreadTableImages() {
        return this.stateThreadTableImages;
    }

    @Override
    public void setStateThreadTableImages(boolean state) {
        this.stateThreadTableImages = state;
    }
}
