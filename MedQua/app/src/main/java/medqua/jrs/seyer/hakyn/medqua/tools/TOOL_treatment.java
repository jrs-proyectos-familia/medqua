package medqua.jrs.seyer.hakyn.medqua.tools;

import org.apache.commons.text.WordUtils;

import java.text.Normalizer;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 2/03/19 a las 09:33 PM
 * @license Joaquin Reyes Sanchez
 */

public class TOOL_treatment {

    /**
     *
     * @param data El dato a iniciar tratamiento
     * @return El img_nuevo dato tratado
     */
    public static String trim (String data) {
        if (data != null)
            return data.trim();
        return data;
    }

    /**
     *
     * @param data El dato a iniciar tratamiento
     * @param type El tipo de texto a cambiar: lowercase, uppercase, capitalizeFully, normal,
     * @return El img_nuevo dato tratado
     */
    public static String text (String data, String type) {
        if (data != null) {
            String newData = null;

            switch (type) {
                case "lowercase":
                    newData = data.toLowerCase();
                    break;
                case "uppercase":
                    newData = data.toUpperCase();
                    break;
                case "capitalizeFully":
                    newData = WordUtils.capitalizeFully(data);
                    break;
                default:
                    newData = data;
            }

            return newData;
        }

        return data;
    }

    /**
     *
     * @param data El dato a iniciar tratamiento
     * @return El img_nuevo dato tratado
     */
    public static String accents (String data) {
        if (data != null) {
            String newData = null;

            newData = Normalizer.normalize(data, Normalizer.Form.NFD);
            newData = newData.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");

            return newData;
        }

        return data;
    }

    /**
     *
     * @param data El dato a iniciar tratamiento
     * @param text El tipo de texto a cambiar: lowercase, uppercase, capitalizeFully
     * @return El img_nuevo dato tratado
     */
    public static String trimTextAccents (String data, String text) {
        if (data != null) {
            String newData = null;

            newData = TOOL_treatment.trim(data);
            newData = TOOL_treatment.text(newData, text);
            newData = TOOL_treatment.accents(newData);

            return newData;
        }

        return data;
    }

}
