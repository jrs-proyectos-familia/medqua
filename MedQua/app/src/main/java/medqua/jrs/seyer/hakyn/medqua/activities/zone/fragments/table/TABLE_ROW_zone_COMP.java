package medqua.jrs.seyer.hakyn.medqua.activities.zone.fragments.table;

import android.content.Context;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import medqua.jrs.seyer.hakyn.medqua.R;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 6/03/19 a las 09:48 AM
 * @license Joaquin Reyes Sanchez
 */

public class TABLE_ROW_zone_COMP {

    private TextView tl_zoneInfoId, tl_zoneTitle, tl_zoneInfoTitle;
    private ImageView iv_zoneImage;

    private ImageButton btnimg_editZone, btnimg_delete, btnimg_editImage;

    private View view;

    public TABLE_ROW_zone_COMP(Context ctx, View v) {
        this.view = v;

        this.iv_zoneImage = v.findViewById(R.id.list_imageBasic_A_image);
        this.tl_zoneTitle = v.findViewById(R.id.list_imageBasic_A_title);
        this.tl_zoneInfoTitle = v.findViewById(R.id.list_imageBasic_A_infoTitle);
        this.tl_zoneInfoId = v.findViewById(R.id.list_imageBasic_A_infoText);
        this.btnimg_editZone = v.findViewById(R.id.list_imageBasic_A_button_A);
        this.btnimg_editImage = v.findViewById(R.id.list_imageBasic_A_button_B);
        this.btnimg_delete = v.findViewById(R.id.list_imageBasic_A_button_C);

        new TABLE_ROW_zone_LIST(this, ctx);
    }

    public TextView getTl_zoneInfoId() {
        return tl_zoneInfoId;
    }

    public TextView getTl_zoneInfoTitle() {
        return tl_zoneInfoTitle;
    }

    public TextView getTl_zoneTitle() {
        return tl_zoneTitle;
    }

    public ImageView getIv_zoneImage() {
        return iv_zoneImage;
    }

    public ImageButton getBtnimg_editZone() {
        return btnimg_editZone;
    }

    public ImageButton getBtnimg_editImage() {
        return btnimg_editImage;
    }

    public ImageButton getBtnimg_delete() {
        return btnimg_delete;
    }

    public View getView() {
        return view;
    }
}
