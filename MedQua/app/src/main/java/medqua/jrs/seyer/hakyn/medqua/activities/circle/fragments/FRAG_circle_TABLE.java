package medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.table.TABLE_circle_COMP;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_TABLE;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 15/03/19 a las 08:30 AM
 * @license Joaquin Reyes Sanchez
 */

public class FRAG_circle_TABLE extends Fragment {

    private Context ctx;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.circle_list_circles, container, false);

        this.ctx = view.getContext();
        this.view = view;

        TABLE_circle_COMP.init(this.ctx, this.view);

        MIX_circle_TABLE.chargeTable(this.ctx);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        TABLE_circle_COMP.init(null, null).clearSingleton();
    }
}
