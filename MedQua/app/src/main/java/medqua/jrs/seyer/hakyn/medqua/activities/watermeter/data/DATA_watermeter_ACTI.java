package medqua.jrs.seyer.hakyn.medqua.activities.watermeter.data;

import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_variablesFragment;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 27/03/19 a las 08:36 PM
 * @license Joaquin Reyes Sanchez
 */

public interface DATA_watermeter_ACTI extends FRAME_variablesFragment {

    DATA_watermeter getWatermeterTemp();
    void setWatermeterTemp(DATA_watermeter watermeter);

}
