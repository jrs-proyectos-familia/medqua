package medqua.jrs.seyer.hakyn.medqua.activities.circle.fragments.table;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_RESETS;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_TABLE;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_VARIABLES;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle_ACTI;
import medqua.jrs.seyer.hakyn.medqua.bd.BD_management;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_menuFragment;
import medqua.jrs.seyer.hakyn.medqua.mixins.modals.MIX_MODA_removeRegister;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 15/03/19 a las 08:44 AM
 * @license Joaquin Reyes Sanchez
 */

public class TABLE_ROW_circle_LIST {

    private Context ctx;
    private TABLE_ROW_circle_COMP tableCOMP;

    private DATA_circle_ACTI circleVARS;
    private FRAME_menuFragment circleMENU;

    public TABLE_ROW_circle_LIST(Context ctx, TABLE_ROW_circle_COMP tableCOMP) {
        this.ctx = ctx;
        this.tableCOMP = tableCOMP;

        this.circleVARS = MIX_circle_VARIABLES.getVariablesFromMyActivity(ctx);
        this.circleMENU = MIX_circle_VARIABLES.getMenuFromMyActivity(ctx);

        new Controls();
    }

    private class Controls {
        private Controls () {
            this.edit();
            this.delete();
        }

        private void edit () {
            TABLE_ROW_circle_LIST
                    .this
                    .tableCOMP
                    .getBtnimg_edit()
                    .setOnClickListener((View v) -> {
                        TABLE_ROW_circle_LIST.this.circleVARS.setCircleTemp((DATA_circle)v.getTag());
                        TABLE_ROW_circle_LIST.this.circleVARS.setTypeRegister("TVS_UPDATE");

                        TABLE_ROW_circle_LIST.this.circleMENU.menuFragment("MENU_FORM");
                    });
        }

        private void delete () {
            TABLE_ROW_circle_LIST
                    .this
                    .tableCOMP
                    .getBtnimg_delete()
                    .setOnClickListener((View v) -> {
                        TABLE_ROW_circle_LIST.this.circleVARS.setCircleTemp((DATA_circle)v.getTag());

                        MIX_MODA_removeRegister<DATA_circle> removeDialog = new MIX_MODA_removeRegister<DATA_circle>() {
                            @Override
                            public void removeRegister(Context ctx, DATA_circle data, AlertDialog dialog) {
                                dialog.dismiss();

                                BD_management manaBD = BD_management.init();

                                if (manaBD.DAO_Circle(ctx).delete(data)) {
                                    MIX_circle_RESETS.resetTable();

                                    MIX_circle_TABLE.chargeTable(ctx);
                                }
                            }
                        };

                        removeDialog.dialogRemove(TABLE_ROW_circle_LIST.this.ctx, TABLE_ROW_circle_LIST.this.circleVARS.getCircleTemp());
                    });
        }
    }


}
