package medqua.jrs.seyer.hakyn.medqua.activities.zone.tvs.rules;

import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_rules;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 8/03/19 a las 05:16 PM
 * @license Joaquin Reyes Sanchez
 */

public class RULE_zone_ZONE extends FRAME_rules {

    public RULE_zone_ZONE() {
        this.nameRule = "zone";

        this.treatTrim = true;
        this.treatTypeText = "capitalizeFully";
        this.treatSpecialCharacters = true;

        this.valEmpty = true;
        this.valMin = 3;
        this.valMax = 25;
        this.valUnique = true;
    }

}
