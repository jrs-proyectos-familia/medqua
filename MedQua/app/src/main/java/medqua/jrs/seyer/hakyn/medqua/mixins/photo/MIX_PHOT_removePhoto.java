package medqua.jrs.seyer.hakyn.medqua.mixins.photo;

import android.content.Context;
import android.widget.Toast;
import medqua.jrs.seyer.hakyn.medqua.R;

import java.io.File;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 2/03/19 a las 07:52 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_PHOT_removePhoto {

    /**
     *
     * @param route Ruta de la imagen a Eliminar
     * @param ctx Contexto de la Activity
     * @param showToast True -> Muestra un Toast cuando la imagen ha sido eliminada, False -> No muestra nada
     * @return boolean dependiendo de si la imagen fue eliminada o no
     */
    public static boolean remove (String route, Context ctx, boolean showToast) {
        boolean success = false;

        try {
            File memory = MIX_PHOT_getMemory.sdCard(ctx);

            File photo = new File(memory, route);

            if (photo.exists())
                if (photo.delete()) {
                    success = true;

                    if (showToast)
                        Toast.makeText(ctx, R.string.toast_image_remove, Toast.LENGTH_LONG).show();
                }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return success;
    }

}
