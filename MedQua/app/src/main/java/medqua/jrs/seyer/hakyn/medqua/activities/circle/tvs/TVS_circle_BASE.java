package medqua.jrs.seyer.hakyn.medqua.activities.circle.tvs;

import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_tvs;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 02:11 PM
 * @license Joaquin Reyes Sanchez
 */

public interface TVS_circle_BASE extends FRAME_tvs {
}
