package medqua.jrs.seyer.hakyn.medqua.frame;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 2/03/19 a las 08:34 AM
 * @license Joaquin Reyes Sanchez
 */

public interface FRAME_menuFragment {

    /**
     *
     * @param fragment Nombre del fragmento a renderizar
     */
    void menuFragment(String fragment);

}
