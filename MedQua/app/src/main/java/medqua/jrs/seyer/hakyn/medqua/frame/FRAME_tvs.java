package medqua.jrs.seyer.hakyn.medqua.frame;

import android.content.Context;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 4/03/19 a las 10:11 AM
 * @license Joaquin Reyes Sanchez
 */

public interface FRAME_tvs {

    void treatment ();
    boolean validation ();

    /**
     *
     * @param ctx Indica el Contexto de la Activity
     * @return
     */
    boolean server (Context ctx);

}
