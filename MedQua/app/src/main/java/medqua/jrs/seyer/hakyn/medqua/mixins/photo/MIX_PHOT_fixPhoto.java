package medqua.jrs.seyer.hakyn.medqua.mixins.photo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 1/03/19 a las 07:59 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_PHOT_fixPhoto {

    /**
     *
     * @param ctx Contexto de la Activity
     * @param uriPhoto Uri de la imagen
     * @return Int
     */
    public static int rotateFixPhoto (Context ctx, Uri uriPhoto) {
        int rotate = 0;

        try {
            ctx.getContentResolver().notifyChange(uriPhoto, null);
            ExifInterface exif = new ExifInterface(uriPhoto.getPath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rotate;
    }

    /**
     *
     * @param routeImage Indica la ruta completa SDCARD de la imagen absoluta donde se encuentra la imagen
     * @param width Indica el ancho deseado para la imagen
     * @param height Indica el alto deseado para la imagen
     * @return Bitmap con la imagen ya escalada
     */
    public static Bitmap scalePhoto (String routeImage, int width, int height) {
        Bitmap imageScale = null;

        try {
            Bitmap photo = BitmapFactory.decodeFile(routeImage);

            if (photo != null) {
                if (width == 0 && height == 0)
                    imageScale = photo;
                else imageScale = Bitmap.createScaledBitmap(photo, width, height, true);
            }

        } catch (Exception e){
            e.printStackTrace();
        }

        return imageScale;
    }

    /**
     *
     * @param photo Indica la imagen en forma de BitImage
     * @param width Indica el ancho de la imagen deseada
     * @param height Indica el alto de la imagen deseada
     * @return Bitmap de la imagen escalada
     */
    public static Bitmap scalePhoto (Bitmap photo, int width, int height) {
        Bitmap imageScale = null;

        try {
            if (width == 0 && height == 0)
                imageScale = photo;
            else imageScale = Bitmap.createScaledBitmap(photo, width, height, true);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return imageScale;
    }

}
