package medqua.jrs.seyer.hakyn.medqua.activities.watermeter.dao;

import medqua.jrs.seyer.hakyn.medqua.activities.watermeter.data.DATA_watermeter;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_dao;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 27/03/19 a las 08:42 PM
 * @license Joaquin Reyes Sanchez
 */

public interface DAO_watermeter_BASE extends FRAME_dao<DATA_watermeter> {
}
