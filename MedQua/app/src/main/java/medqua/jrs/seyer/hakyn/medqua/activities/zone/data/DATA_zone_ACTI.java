package medqua.jrs.seyer.hakyn.medqua.activities.zone.data;

import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_variablesFragment;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 6/03/19 a las 07:10 PM
 * @license Joaquin Reyes Sanchez
 */

public interface DATA_zone_ACTI extends FRAME_variablesFragment {

    DATA_zone getZoneTemp ();
    void setZoneTemp (DATA_zone zone);

    boolean getStateThreadTableImages();
    void setStateThreadTableImages(boolean state);

}
