package medqua.jrs.seyer.hakyn.medqua.activities.zone.dao;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 4/03/19 a las 02:15 PM
 * @license Joaquin Reyes Sanchez
 */

public interface DAO_zone_SQL {

    String sql_readAllZone = "SELECT * FROM zone ORDER BY zone ASC";

}
