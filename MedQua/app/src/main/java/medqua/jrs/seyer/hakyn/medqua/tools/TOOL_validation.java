package medqua.jrs.seyer.hakyn.medqua.tools;

import android.content.Context;
import medqua.jrs.seyer.hakyn.medqua.R;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 3/03/19 a las 08:17 PM
 * @license Joaquin Reyes Sanchez
 */

public class TOOL_validation<T> {

    private Context ctx = null;

    /**
     *
     * @param ctx Contexto de la Activity
     */
    public TOOL_validation(Context ctx) {
        this.ctx = ctx;
    }

    /**
     *
     * @param data Dato correspondiente a validar
     * @return El error si es que éxiste
     */

    public String isEmptyString (String data) {
        String error = this.ctx.getString(R.string.validator_empty);

        if (data.equals("")) return error;

        return "";
    }

    /**
     *
     * @param data Dato correspondiente a validar
     * @param min Rango mínimo permítido
     * @return El error si es que éxiste
     */
    public String minString (String data, int min) {
        int missingCharacters = min - data.length();
        String error = this.ctx.getString(R.string.validator_min).replaceAll("#####", String.valueOf(missingCharacters));

        if (data.length() < min) return error;

        return "";
    }

    /**
     *
     * @param data Dato correspondiente a validar
     * @param max Rango máximo permítido
     * @return El error si es que éxiste
     */
    public String maxString (String data, int max) {
        int exceededCharacters = data.length() - max;
        String error = this.ctx.getString(R.string.validator_max).replaceAll("#####", String.valueOf(exceededCharacters));

        if (data.length() > max) return error;

        return "";
    }

    /**
     *
     * @param data Dato correspondiente a validar
     * @param noValid Numero no permitido en la selección
     * @return El error si es que éxiste
     */
    public String selectEmptyInt (int data, int noValid) {
        String error = this.ctx.getString(R.string.validator_select_empty);

        if (data == noValid) return error;

        return "";
    }

    /**
     *
     * @param original Dato a comparar
     * @param compare Dato Original
     * @param nameFieldCompare Nombre del campo del dato original
     * @return El error si es que éxiste
     */
    public String noEqual (String original, String compare, String nameFieldCompare) {
        String error = this.ctx.getString(R.string.validator_noEqual);

        if (compare == null && original == null) return TOOL_text.findAndReplaceAll("##", nameFieldCompare, error);

        if (compare == null && original != null) return "";

        if (compare.equals(original)) return TOOL_text.findAndReplaceAll("##", nameFieldCompare, error);

        return "";
    }

    /**
     *
     * @param original Dato a comparar
     * @param compare Dato Original
     * @param nameFieldCompare Nombre del campo del dato original
     * @return El error si es que éxiste
     */
    public String noEqual (int original, int compare, String nameFieldCompare) {
        String error = this.ctx.getString(R.string.validator_noEqual);

        if (original == compare) return TOOL_text.findAndReplaceAll("##", nameFieldCompare, error);

        return "";
    }

// Validaciones Personalizadas

    /**
     *
     * @param data Dato correspondiente a validar
     * @param min Rango mínimo permítido
     * @param max Rango máximo permítido
     * @return El error si es que éxiste
     */
    public String valEmptyMinMax (String data, int min, int max) {
        String valError = "";

        valError = this.isEmptyString(data);
        if (!valError.equals("")) return valError;
        else {
            valError = this.minString(data, min);
            if (!valError.equals("")) return valError;
            else {
                valError = this.maxString(data, max);
                if (!valError.equals("")) return valError;
                else return "";
            }
        }
    }

    /**
     *
     * @param data Dato correspondiente a validar
     * @param min Rango mínimo permítido
     * @param max Rango máximo permítido
     * @return El error si es que éxiste
     */
    public String valOptionalEmptyMinMax (String data, int min, int max) {
        String valError = "";

        valError = this.isEmptyString(data);
        if (valError.equals("")) {
            valError = this.minString(data, min);
            if (!valError.equals("")) return valError;
            else {
                valError = this.maxString(data, max);
                if (!valError.equals("")) return valError;
                else return "";
            }
        }

        return "";
    }

    /**
     *
     * @param type Indica el tipo de mensaje de error que se desea obtener
     * @return String Mensaje del error a desear
     */
    public String getError (String type) {
        String error = "";

        switch (type) {
            case "unique":
                error = this.ctx.getString(R.string.validator_unique);
                break;
        }

        return error;
    }

}
