package medqua.jrs.seyer.hakyn.medqua.activities.circle.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle;
import medqua.jrs.seyer.hakyn.medqua.bd.BD_sqlite;

import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 03:58 PM
 * @license Joaquin Reyes Sanchez
 */

public class DAO_circle implements DAO_circle_BASE {
    private Context ctx;
    private BD_sqlite bdSqlite;

    public DAO_circle (Context ctx) {
        this.ctx = ctx;
        this.bdSqlite = new BD_sqlite(this.ctx, "medqua", null, 1);
    }

    @Override
    public boolean create(DATA_circle data) {
        boolean success = false;

        SQLiteDatabase db = this.bdSqlite.getWritableDatabase();

        ContentValues newCircle = new ContentValues();
        newCircle.put("circle", data.get_circle());

        if (db.insert("circle", null, newCircle) != -1) {
            String messageToast = this.ctx.getResources().getString(R.string.dataBase_created).replaceAll("#####", data.get_circle());

            Toast.makeText(this.ctx, messageToast, Toast.LENGTH_SHORT).show();

            success = true;
        }

        db.close();

        return success;
    }

    @Override
    public boolean update(DATA_circle data) {
        boolean success = false;

        SQLiteDatabase db = this.bdSqlite.getWritableDatabase();

        ContentValues updateCircle = new ContentValues();
        updateCircle.put("circle", data.get_circle());

        if (db.update("circle", updateCircle, "id = " + data.get_id(), null) > 0) {
            String messageToast = this.ctx.getResources().getString(R.string.dataBase_updated).replaceAll("#####", data.get_circle());

            Toast.makeText(this.ctx, messageToast, Toast.LENGTH_SHORT).show();

            success = true;
        }

        db.close();

        return success;
    }

    @Override
    public boolean delete(DATA_circle data) {
        boolean success = false;

        SQLiteDatabase db = this.bdSqlite.getWritableDatabase();

        if (db.delete("circle", "id = " + data.get_id(), null) > 0) {
            String messageToast =  this.ctx.getResources().getString(R.string.dataBase_removed).replaceAll("#####", data.get_circle());

            Toast.makeText(this.ctx, messageToast, Toast.LENGTH_SHORT).show();
            success = true;
        }

        db.close();

        return success;
    }

    @Override
    public ArrayList<DATA_circle> read() {
        ArrayList<DATA_circle> listCircles = new ArrayList<>();

        SQLiteDatabase db = this.bdSqlite.getWritableDatabase();

        Cursor cursor = db.rawQuery(DAO_circle_SQL.sql_readAllCircle, null);

        if (cursor.moveToFirst()) {
            do {
                DATA_circle circle = new DATA_circle();
                circle.set_id(cursor.getInt(0));
                circle.set_circle(cursor.getString(1));

                listCircles.add(circle);
            } while(cursor.moveToNext());
        }

        db.close();

        return listCircles;
    }

    @Override
    public boolean valUnique(String sql) {
        boolean unique = false;

        SQLiteDatabase db = this.bdSqlite.getWritableDatabase();

        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.moveToFirst())
            unique = true;

        db.close();

        return unique;
    }

    @Override
    public void setContext(Context ctx) {
        this.ctx = ctx;
    }
}
