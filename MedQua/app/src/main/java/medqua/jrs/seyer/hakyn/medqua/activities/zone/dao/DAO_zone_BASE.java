package medqua.jrs.seyer.hakyn.medqua.activities.zone.dao;

import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_dao;

import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 3/03/19 a las 10:42 PM
 * @license Joaquin Reyes Sanchez
 */

public interface DAO_zone_BASE<T> extends FRAME_dao<T> {

    boolean updateZone(T data);
    boolean updateImage(T data);
    ArrayList<T> readWithImages ();

}
