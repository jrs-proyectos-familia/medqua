package medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins;

import android.app.Activity;
import android.content.Context;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle_ACTI;
import medqua.jrs.seyer.hakyn.medqua.frame.FRAME_menuFragment;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 13/03/19 a las 11:09 AM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_circle_VARIABLES {

    public static DATA_circle_ACTI getVariablesFromMyActivity (Context ctx) {
        Activity activity = (Activity)ctx;
        return (DATA_circle_ACTI) activity;
    }

    public static FRAME_menuFragment getMenuFromMyActivity (Context ctx) {
        Activity activity = (Activity)ctx;
        return (FRAME_menuFragment)activity;
    }

}
