package medqua.jrs.seyer.hakyn.medqua.activities.zone.data;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 3/03/19 a las 08:39 PM
 * @license Joaquin Reyes Sanchez
 */

public class DATA_zone {

    private Integer id = -1;
    private String zone = "";
    private String image = "";

    public DATA_zone(int id, String zone, String image) {
        this.id = id;
        this.zone = zone;
        this.image = image;
    }

    public DATA_zone() {}

    public Integer get_id() {
        return id;
    }

    public void set_id(Integer id) {
        this.id = id;
    }

    public String get_zone() {
        return zone;
    }

    public void set_zone(String zone) {
        this.zone = zone;
    }

    public String get_image() {
        return image;
    }

    public void set_image(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "DATA_zone{" +
                "id=" + id +
                ", zone='" + zone + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
