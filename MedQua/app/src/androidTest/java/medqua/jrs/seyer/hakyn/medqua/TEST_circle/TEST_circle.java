package medqua.jrs.seyer.hakyn.medqua.TEST_circle;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import medqua.jrs.seyer.hakyn.medqua.TEST_circle.tests.control.CONTROL_circleACTIONS;
import medqua.jrs.seyer.hakyn.medqua.TEST_circle.tests.form.FORM_circleACTIONS;
import medqua.jrs.seyer.hakyn.medqua.activities.ACTI_circle;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle;
import medqua.jrs.seyer.hakyn.medqua.bd.request.REQ_circle;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 15/03/19 a las 12:25 PM
 * @license Joaquin Reyes Sanchez
 */

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TEST_circle {

	private String[] circles = new String[]{
		"Lecturista",
		"Administrador",
		"Testeador",
		"Secretario"
	};

	private ArrayList<DATA_circle> dataCircles() {
		return REQ_circle.getAllCircles(ruleCircleACTI.getActivity().getWindow().getContext());
	}

	@Rule
	public ActivityTestRule<ACTI_circle> ruleCircleACTI = new ActivityTestRule<>(ACTI_circle.class);

	@Test
	public void TEST_MAIN () {
		this.TEST_A_cleanCircle();
		this.TEST_B_newCircle();
		this.TEST_C_updateCircle();
		this.TEST_D_removeCircle();
	}

	@Test
	public void TEST_A_cleanCircle () {
		for (String text : this.circles)
			CONTROL_circleACTIONS.clearForm(text);
	}

	@Test
	public void TEST_B_newCircle() {
		for (String text : this.circles)
			FORM_circleACTIONS.newCircle(ruleCircleACTI.getActivity(), text);
	}

	@Test
	public void TEST_C_updateCircle() {
		ArrayList<DATA_circle> circles = this.dataCircles();

		for (DATA_circle circle : circles)
			FORM_circleACTIONS.updateCircle(ruleCircleACTI.getActivity(), circle.get_circle(), false);
	}

	@Test
	public void TEST_D_removeCircle() {
		ArrayList<DATA_circle> circles = this.dataCircles();

		for (DATA_circle circle : circles)
			FORM_circleACTIONS.removeCircle(ruleCircleACTI.getActivity(), circle.get_circle(), false);
	}
}
