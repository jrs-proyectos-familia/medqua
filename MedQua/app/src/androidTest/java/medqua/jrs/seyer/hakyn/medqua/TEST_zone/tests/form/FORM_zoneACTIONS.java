package medqua.jrs.seyer.hakyn.medqua.TEST_zone.tests.form;

import android.app.Activity;
import android.support.test.espresso.ViewInteraction;
import android.view.View;
import medqua.jrs.seyer.hakyn.medqua.mixins.MIX_TEST_toast;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.TEST_zone.COMP_zone;
import medqua.jrs.seyer.hakyn.medqua.TEST_zone.mixins.MIX_imageIV;
import medqua.jrs.seyer.hakyn.medqua.TEST_zone.tests.control.CONTROL_zoneCLEAN;
import medqua.jrs.seyer.hakyn.medqua.TEST_zone.tests.panel.PANEL_zoneACTIONS;
import medqua.jrs.seyer.hakyn.medqua.bd.BD_management;
import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 21/03/19 a las 12:02 PM
 * @license Joaquin Reyes Sanchez
 */

public class FORM_zoneACTIONS extends COMP_zone {

	public static void newZone(Activity activity, String textZone) {
		FORM_zoneVAL.runVal_AllEmpties(activity);

		FORM_zoneVAL.runVal_MinZone(activity, textZone);

		FORM_zoneVAL.runVal_MaxZone(activity, textZone);

		BD_management manaBD = BD_management.init();

		if (manaBD.DAO_Zone(activity.getWindow().getContext()).valUnique("SELECT id FROM zone WHERE zone = \"" + textZone + "\"")) {
			FORM_zoneVAL.runVal_UniqueZone(activity, textZone);

			// AQUI SE ELIMINARÁ EL REGISTRO
			removeZone(activity, textZone, false);

			CONTROL_zoneCLEAN.clearForm();
		}

		FORM_zoneVAL.valSuccessForm(activity, textZone);

		PANEL_zoneACTIONS.changeFragment(activity, "MENU_TABLE");

		// Aqui reviso si se creo la zona nueva en la tabla
		onView(allOf(withText(textZone), isDescendantOfA(withId(R.id.list_zones))))
			.check(matches(isDisplayed()));

		PANEL_zoneACTIONS.changeFragment(activity, "MENU_FORM");

		CONTROL_zoneCLEAN.checkCleanFields();
		CONTROL_zoneCLEAN.checkCleanErrors();
	}

	private static ViewInteraction table_chargeZoneForEditIt(Activity activity, Matcher<View> buttonEdit, String textZone) {
		PANEL_zoneACTIONS.changeFragment(activity, "MENU_TABLE");

		// Encuentro el elemento de la lista (tabla) al cual deseo realizarle una actualización
		return onView(
			allOf(
				buttonEdit,
				isDescendantOfA(
					hasSibling(
						allOf(
							withText(textZone),
							isDescendantOfA(
								withId(R.id.list_zones))
						)
					)
				)
			)
		).check(matches(isDisplayed()));
	}

	private static void check_stateForInputsDependingUpdateAction(String textZone, String updateAction) {
		// Valido si el botón de nuevo registro esta visible y además que este cargada la información
		onView(COMP_zone.FORM_CONTROL_newRegister)
			.check(matches(isDisplayed()));

		CONTROL_zoneCLEAN.checkCleanFields(textZone, "UPDATE");

		// Valido que el botón de cargar imagen este desactivado
		if (updateAction.equals("zone"))
			onView(COMP_zone.FORM_ACTION_image)
				.check(matches(not(isEnabled())));
		else if (updateAction.equals("image"))
			onView(COMP_zone.FORM_INPUT_zone)
				.check(matches(not(isEnabled())));
	}

	private static void sendCheck_updateRegister(Activity activity, String textZoneUpdated, boolean checkOriginal) {
		// Envio los datos para actualizar
		onView(COMP_zone.FORM_CONTROL_sendData)
			.perform(click());

		// Evalúo si se desplegó el TOAST sobre si se actualizó correctamente el registro
		onView(withText(activity.getResources().getString(R.string.dataBase_updated).replace("#####", textZoneUpdated)))
			.inRoot(new MIX_TEST_toast())
			.check(matches(isDisplayed()));

		// Valido si mi vista se encuentra correctamente lista como para realizar un nuevo registro 2do Intento
		CONTROL_zoneCLEAN.checkCleanFields();
		CONTROL_zoneCLEAN.checkCleanErrors();
		onView(COMP_zone.PANEL_newRegister)
			.check(matches(withText(R.string.zone_panel_newRegister)));

		// Reviso si existe la actualización creada
		table_chargeZoneForEditIt(activity, withId(R.id.list_imageBasic_A_button_A), textZoneUpdated);

		// Reviso si no existe el registro original
		if (checkOriginal) {
			onView(
				allOf(
					withText(textZoneUpdated),
					isDescendantOfA(withId(R.id.list_zones))
				)
			)
				.check(doesNotExist());
		}

		PANEL_zoneACTIONS.changeFragment(activity, "MENU_FORM");
	}

	public static void updateZone(Activity activity, String textZone, boolean restart) {
		String updatedTextZone = " Eliminado";

		// Compruebo si mi nuevo cambio no excede el tamaño máximo permitido
		int sizeNewText = (textZone + updatedTextZone).length();
		if (sizeNewText > COMP_zone.RULE_zone.getValMax()) {
			int removeLetters = sizeNewText - COMP_zone.RULE_zone.getValMax();

			String fixUpdatedTextZone = "";

			for (int i = 0; i < removeLetters; i++)
				fixUpdatedTextZone += updatedTextZone.charAt(i);

			updatedTextZone = fixUpdatedTextZone;
		}

		table_chargeZoneForEditIt(activity, withId(R.id.list_imageBasic_A_button_A), textZone)
			.perform(click());

		check_stateForInputsDependingUpdateAction(textZone, "zone");

		PANEL_zoneACTIONS.changeFragment(activity, "MENU_FORM");

		check_stateForInputsDependingUpdateAction(textZone, "zone");

		// Hago click en el botón de nuevo registro
		onView(COMP_zone.FORM_CONTROL_newRegister)
			.perform(click());
		// Valido si mi vista se encuentra correctamente lista como para realizar un nuevo registro 1er Intento
		CONTROL_zoneCLEAN.checkCleanFields();
		CONTROL_zoneCLEAN.checkCleanErrors();
		onView(COMP_zone.PANEL_newRegister)
			.check(matches(withText(R.string.zone_panel_newRegister)));

		// [TIP]: Aqui podría en teoría realizar un nuevo registro

		table_chargeZoneForEditIt(activity, withId(R.id.list_imageBasic_A_button_A), textZone)
			.perform(click());

		// Envio los datos sin modificar
		onView(COMP_zone.FORM_CONTROL_sendData)
			.perform(click());
		// Evalúo si se mostró el Toast sobre que no se ha realizado ningún cambio con respecto al original
		onView(withText(R.string.toast_dataTemp_noChanges))
			.inRoot(new MIX_TEST_toast())
			.check(matches(isDisplayed()));

		// Realizo un cambio en la zona 1er Intento
		onView(COMP_zone.FORM_INPUT_zone)
			.perform(typeText(updatedTextZone))
			.perform(closeSoftKeyboard());

		// Realizo una limpieza para ver si se restaura a su estado original
		CONTROL_zoneCLEAN.clearForm(textZone, "UPDATE");

		// Realizo un cambio en la zona 2do Intento
		onView(COMP_zone.FORM_INPUT_zone)
			.perform(typeText(updatedTextZone))
			.perform(closeSoftKeyboard());

		// Envio y Evaluo los resultados
		sendCheck_updateRegister(activity, textZone + updatedTextZone, false);

		// Realizo una restauración de los datos a su estado original siempre y cuando se desee
		if (restart) {
			removeZone(activity, textZone + updatedTextZone, false);

			newZone(activity, textZone);
		}
	}

	public static void updateImage(Activity activity, String textZone) {
		table_chargeZoneForEditIt(activity, withId(R.id.list_imageBasic_A_button_B), textZone)
			.perform(click());

		check_stateForInputsDependingUpdateAction(textZone, "image");

		// Hago click en el botón de nuevo registro
		onView(COMP_zone.FORM_CONTROL_newRegister)
			.perform(click());
		// Valido si mi vista se encuentra correctamente lista como para realizar un nuevo registro 1er Intento
		CONTROL_zoneCLEAN.checkCleanFields();
		CONTROL_zoneCLEAN.checkCleanErrors();
		onView(COMP_zone.PANEL_newRegister)
			.check(matches(withText(R.string.zone_panel_newRegister)));

		// [TIP]: Aqui podría en teoría realizar un nuevo registro

		table_chargeZoneForEditIt(activity, withId(R.id.list_imageBasic_A_button_B), textZone)
			.perform(click());

		MIX_imageIV.chargeImageFromGallery(activity, false);

		sendCheck_updateRegister(activity, textZone, false);
	}

	public static void removeZone(Activity activity, String textZone, boolean testCancel) {
		table_chargeZoneForEditIt(activity, withId(R.id.list_imageBasic_A_button_C), textZone)
			.perform(click());

		if (testCancel) {
			onView(withText(R.string.modal_removeRegister_cancel))
				.inRoot(isDialog())
				.check(matches(isDisplayed()))
				.perform(click());

			table_chargeZoneForEditIt(activity, withId(R.id.list_imageBasic_A_button_C), textZone)
				.perform(click());
		}

		onView(withText(R.string.modal_removeRegister_remove))
			.inRoot(isDialog())
			.check(matches(isDisplayed()))
			.perform(click());

		onView(withText(activity.getResources().getString(R.string.dataBase_removed).replace("#####", textZone)))
			.inRoot(new MIX_TEST_toast())
			.check(matches(isDisplayed()));

		PANEL_zoneACTIONS.changeFragment(activity, "MENU_FORM");
	}

}
