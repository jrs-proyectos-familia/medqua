package medqua.jrs.seyer.hakyn.medqua.TEST_zone;

import android.view.View;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.tvs.rules.RULE_zone_IMAGE;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.tvs.rules.RULE_zone_ZONE;
import org.hamcrest.Matcher;

import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 20/03/19 a las 12:30 PM
 * @license Joaquin Reyes Sanchez
 */

public class COMP_zone {

	// DEL FORMULARIO
	// Inputs
	protected static Matcher<View> FORM_INPUT_zone = withId(R.id.form_zone_input_zone);
	protected static Matcher<View> FORM_INPUT_image = withId(R.id.form_zone_input_image);
	// Actions
	protected static Matcher<View> FORM_ACTION_image = withId(R.id.form_zone_action_image);
	// Errors
	protected static Matcher<View> FORM_ERROR_zone = withId(R.id.form_zone_error_zone);
	protected static Matcher<View> FORM_ERROR_image = withId(R.id.form_zone_error_image);

	// Controls
	protected static Matcher<View> FORM_CONTROL_newRegister = withId(R.id.form_control_newRegister);
	protected static Matcher<View> FORM_CONTROL_cleanForm = withId(R.id.form_control_cleanForm);
	protected static Matcher<View> FORM_CONTROL_sendData = withId(R.id.form_control_sendData);

	// Panel
	protected static Matcher<View> PANEL_newRegister = withId(R.id.panel_newRegister);
	protected static Matcher<View> PANEL_list = withId(R.id.panel_list);

	// Reglas
	protected static RULE_zone_ZONE RULE_zone = new RULE_zone_ZONE();
	protected static RULE_zone_IMAGE RULE_image = new RULE_zone_IMAGE();

}
