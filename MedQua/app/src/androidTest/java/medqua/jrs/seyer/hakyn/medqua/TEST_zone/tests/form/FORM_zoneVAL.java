package medqua.jrs.seyer.hakyn.medqua.TEST_zone.tests.form;

import android.app.Activity;
import medqua.jrs.seyer.hakyn.medqua.mixins.MIX_TEST_toast;
import medqua.jrs.seyer.hakyn.medqua.mixins.MIX_TEST_validation;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.TEST_zone.COMP_zone;
import medqua.jrs.seyer.hakyn.medqua.TEST_zone.mixins.MIX_imageIV;
import medqua.jrs.seyer.hakyn.medqua.TEST_zone.tests.control.CONTROL_zoneCLEAN;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 20/03/19 a las 02:02 PM
 * @license Joaquin Reyes Sanchez
 */

public class FORM_zoneVAL extends COMP_zone {

	public static void valEmptyZone(Activity activity) {
		String errorEmpty = MIX_TEST_validation.getMessageErrorEmpty(activity);
		MIX_TEST_validation.valInputError(null, "", COMP_zone.FORM_ERROR_zone, errorEmpty, COMP_zone.FORM_CONTROL_sendData);

		onView(withText(activity.getResources().getString(R.string.toast_form_missingDataForSend)))
			.inRoot(new MIX_TEST_toast())
			.check(matches(isDisplayed()));
	}

	public static void valMinZone(Activity activity, String text) {
		String textMin = text;
		if (text.length() > COMP_zone.RULE_zone.getValMin())
			textMin = text.charAt(0) + "" + text.charAt(1);
		else if (text.length() == COMP_zone.RULE_zone.getValMin())
			textMin = "HS";

		int missingCharacters = COMP_zone.RULE_zone.getValMin() - textMin.length();
		String errorMin = MIX_TEST_validation.getMessageErrorMin(activity, String.valueOf(missingCharacters));
		MIX_TEST_validation.valInputError(COMP_zone.FORM_INPUT_zone, textMin, COMP_zone.FORM_ERROR_zone, errorMin, COMP_zone.FORM_CONTROL_sendData);
	}

	public static void valMaxZone(Activity activity, String text) {
		String textMax = text;
		if (text.length() < COMP_zone.RULE_zone.getValMax()) {
			int missingText = COMP_zone.RULE_zone.getValMax() - text.length();

			for (int i = 0; i < missingText + 1; i++)
				textMax += "X";
		} else if (text.length() == COMP_zone.RULE_zone.getValMax())
			textMax += "X";
		int exceededCharacters = textMax.length() - COMP_zone.RULE_zone.getValMax();
		String errorMax = MIX_TEST_validation.getMessageErrorMax(activity, String.valueOf(exceededCharacters));

		MIX_TEST_validation.valInputError(COMP_zone.FORM_INPUT_zone, textMax, COMP_zone.FORM_ERROR_zone, errorMax, COMP_zone.FORM_CONTROL_sendData);
	}

	public static void valUniqueZone(Activity activity, String text) {
		String errorUnique = MIX_TEST_validation.getMessageErrorUnique(activity, text);

		MIX_TEST_validation.valInputError(COMP_zone.FORM_INPUT_zone, text, COMP_zone.FORM_ERROR_zone, errorUnique, COMP_zone.FORM_CONTROL_sendData);
	}

	public static void valEmptyImage(Activity activity) {
		String errorEmpty = MIX_TEST_validation.getMessageErrorEmpty(activity);
		// Debido a que es una imagen, no ingresaré nada y solo checaré si se muestra el error al activar el botón de enviar
		MIX_TEST_validation.valInputError(null, "", COMP_zone.FORM_ERROR_image, errorEmpty, COMP_zone.FORM_CONTROL_sendData);

		onView(withText(activity.getResources().getString(R.string.toast_form_missingDataForSend)))
			.inRoot(new MIX_TEST_toast())
			.check(matches(isDisplayed()));
	}

	// TODO: Aqui faltó el test para valUniqueImage, pero debido a que es demasiado difícil que dos o más imagenes sean iguales debido a que se añade el tiempo actual y una llave aleatoria como formá clave única al momento de ser guardadas dentro del dispositivo. De cualquier forma no se descarta para en un futuro realizar un test de esto.

	public static void valSuccessForm(Activity activity, String textZone) {
		MIX_TEST_validation.valInputError(COMP_zone.FORM_INPUT_zone, textZone, COMP_zone.FORM_ERROR_zone, "", null);

		MIX_imageIV.chargeImageFromGallery(activity, true);

		onView(COMP_zone.FORM_CONTROL_sendData)
			.perform(click());

		onView(withText(activity.getResources().getString(R.string.dataBase_created).replace("#####", textZone)))
			.inRoot(new MIX_TEST_toast())
			.check(matches(isDisplayed()));

		CONTROL_zoneCLEAN.checkCleanFields();
		CONTROL_zoneCLEAN.checkCleanErrors();
	}

	public static void runVal_AllEmpties(Activity activity) {
		valEmptyZone(activity);
		valEmptyImage(activity);

		CONTROL_zoneCLEAN.clearForm();
	}

	private static void successData (Activity activity, String noSuccessInput) {
		switch (noSuccessInput) {
			case "zone":
				MIX_imageIV.chargeImageFromGallery(activity, true);
				break;
		}
	}

	public static void runVal_MinZone(Activity activity, String textZone) {
		successData(activity, "zone");

		valMinZone(activity, textZone);

		CONTROL_zoneCLEAN.clearForm();
	}

	public static void runVal_MaxZone(Activity activity, String textZone) {
		successData(activity, "zone");

		valMaxZone(activity, textZone);

		CONTROL_zoneCLEAN.clearForm();
	}

	public static void runVal_UniqueZone(Activity activity, String textZone) {
		successData(activity, "zone");

		valUniqueZone(activity, textZone);

		CONTROL_zoneCLEAN.clearForm();
	}

}
