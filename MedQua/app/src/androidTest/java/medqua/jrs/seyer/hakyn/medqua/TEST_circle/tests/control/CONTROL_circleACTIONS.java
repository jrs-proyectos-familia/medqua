package medqua.jrs.seyer.hakyn.medqua.TEST_circle.tests.control;

import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.TEST_circle.COMP_circle;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 19/03/19 a las 12:08 PM
 * @license Joaquin Reyes Sanchez
 */

public class CONTROL_circleACTIONS extends COMP_circle {

	public static void clearForm(String text) {
		if (!text.equals("")) {
			onView(CONTROL_circleACTIONS.FORM_INPUT_circle)
				.perform(
					typeText(text),
					closeSoftKeyboard()
				)
				.check(
					matches(withText(text))
				);
		}

		onView(CONTROL_circleACTIONS.FORM_CONTROL_cleanForm)
			.perform(
				click()
			);

		onView(CONTROL_circleACTIONS.FORM_INPUT_circle)
			.check(
				matches(withText(""))
			);
	}

	public static void clearFormUpdate(String matchText) {
		onView(CONTROL_circleACTIONS.FORM_CONTROL_cleanForm)
			.perform(
				click()
			);

		onView(CONTROL_circleACTIONS.FORM_INPUT_circle)
			.check(
				matches(withText(matchText))
			);
	}

	public static void isClearForm() {
		onView(CONTROL_circleACTIONS.PANEL_newRegister)
			.check(matches(withText(R.string.circle_panel_newRegister)));

		onView(CONTROL_circleACTIONS.FORM_INPUT_circle)
			.check(matches(withText("")));

		onView(CONTROL_circleACTIONS.FORM_CONTROL_newRegister)
			.check(matches(not(isDisplayed())));
	}

}
