package medqua.jrs.seyer.hakyn.medqua.TEST_circle.tests.form;

import android.app.Activity;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.TEST_circle.COMP_circle;
import medqua.jrs.seyer.hakyn.medqua.mixins.MIX_TEST_toast;
import medqua.jrs.seyer.hakyn.medqua.TEST_circle.tests.control.CONTROL_circleACTIONS;
import medqua.jrs.seyer.hakyn.medqua.TEST_circle.tests.panel.PANEL_circleACTIONS;
import medqua.jrs.seyer.hakyn.medqua.bd.BD_management;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.allOf;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 19/03/19 a las 12:11 PM
 * @license Joaquin Reyes Sanchez
 */

public class FORM_circleACTIONS extends COMP_circle {

    public static void newCircle (Activity activity, String text) {
        FORM_circleVAL.valEmptyCircle(activity, text);

        FORM_circleVAL.valMinCircle(activity, text);

        FORM_circleVAL.valMaxCircle(activity, text);

        BD_management manaBD = BD_management.init();
        if (manaBD.DAO_Circle(activity.getWindow().getContext()).valUnique("SELECT id FROM circle WHERE circle = \"" + text + "\"")) {
            FORM_circleVAL.valUniqueCircle(activity, text);

            // AQUI VAMOS A ELIMINAR EL TARGET
            removeCircle(activity, text, true);

            CONTROL_circleACTIONS.clearForm("");
        }

        FORM_circleVAL.valSuccessCircle(activity, text);

        PANEL_circleACTIONS.changeFragment(activity,"MENU_TABLE");

        // Reviso si existe el registro ya creado
        onView(allOf(withText(text), isDescendantOfA(withId(R.id.list_circles))))
            .check(matches(isDisplayed()));

        PANEL_circleACTIONS.changeFragment(activity,"MENU_FORM");

        onView(COMP_circle.FORM_INPUT_circle)
            .check(matches(withText("")));
    }

    public static void updateCircle (Activity activity, String text, boolean restart) {
        String updatedText = " Eliminado";

        PANEL_circleACTIONS.changeFragment(activity,"MENU_TABLE");

        // Selecciono el item de la tabla y hago click en el botón de editar
        onView(allOf(withId(R.id.list_basic_A_button_A), isDescendantOfA(hasSibling(allOf(withText(text), isDescendantOfA(withId(R.id.list_circles)))))))
            .check(matches(isDisplayed()))
            .perform(click());

        // Valido si el botón de nuevo registro esta visible y además reviso si se rellenó el formulario con los datos de la lista
        onView(COMP_circle.FORM_CONTROL_newRegister)
            .check(matches(isDisplayed()));
        onView(COMP_circle.FORM_INPUT_circle)
            .check(matches(withText(text)));

        PANEL_circleACTIONS.changeFragment(activity,"MENU_FORM");

        // Valido si el botón de nuevo registro esta visible y además reviso si se rellenó el formulario con los datos de la lista
        onView(COMP_circle.FORM_CONTROL_newRegister)
            .check(matches(isDisplayed()));
        onView(COMP_circle.FORM_INPUT_circle)
            .check(matches(withText(text)));

        // Activo el botón de nuevo registro con el fin de resetear el fragmento a su estado original
        onView(COMP_circle.FORM_CONTROL_newRegister)
            .perform(click());

        // Valido si el botón de nuevo registro esta invisible y además reviso si el formulario esta limpio
        CONTROL_circleACTIONS.isClearForm();

        PANEL_circleACTIONS.changeFragment(activity,"MENU_TABLE");

        // Selecciono el item de la tabla y hago click en el botón de editar
        onView(allOf(withId(R.id.list_basic_A_button_A), isDescendantOfA(hasSibling(allOf(withText(text), isDescendantOfA(withId(R.id.list_circles)))))))
            .check(matches(isDisplayed()))
            .perform(click());

        // Valido si el botón de nuevo registro esta visible y además reviso si se rellenó el formulario con los datos de la lista
        onView(COMP_circle.FORM_CONTROL_newRegister)
                .check(matches(isDisplayed()));
        onView(COMP_circle.FORM_INPUT_circle)
            .check(matches(withText(text)));

        // Activo el botón de enviar para la acción editar
        onView(COMP_circle.FORM_CONTROL_sendData)
            .check(matches(withText(R.string.action_edit)))
            .perform(click());

        // Evalúo si se mostró el Toast sobre que no se ha realizado ningún cambio con respecto al original
        onView(withText(activity.getResources().getString(R.string.toast_dataTemp_noChanges)))
            .inRoot(new MIX_TEST_toast())
            .check(matches(isDisplayed()));

        // Realizo un cambio en el input de circle
        onView(COMP_circle.FORM_INPUT_circle)
            .perform(
                typeText(text + updatedText),
                closeSoftKeyboard()
            );

        // Restauro el registro a su estado original
        CONTROL_circleACTIONS.clearFormUpdate(text);

        // Realizo un cambio en el input de circle
        onView(COMP_circle.FORM_INPUT_circle)
            .perform(
                typeText(updatedText),
                closeSoftKeyboard()
            );

        // Activo el botón de enviar para la acción editar
        onView(COMP_circle.FORM_CONTROL_sendData)
            .perform(click());

        // Compruebo si no se ha extendido el valor máximo y si lo hace pues lo arreglo para que asi pueda enviarlo
        int sizeUpdatedText = (text + updatedText).length();
        if (sizeUpdatedText > RULE_circle.getValMax()) {
        		int exceededCharacters = sizeUpdatedText - RULE_circle.getValMax();
            String errorMax = activity.getResources().getString(R.string.validator_max).replace("#####", String.valueOf(exceededCharacters));
            onView(COMP_circle.FORM_ERROR_circle)
                .check(matches(withText(errorMax)));

            CONTROL_circleACTIONS.clearFormUpdate(text);

            int missingText = sizeUpdatedText - RULE_circle.getValMax();

            String fixUpdatedText = "";

            for (int i = 0; i < updatedText.length() - missingText; i++)
                fixUpdatedText += updatedText.charAt(i);

            updatedText = fixUpdatedText;

            // Realizo un cambio en el input de circle
            onView(COMP_circle.FORM_INPUT_circle)
                .perform(
                    typeText(updatedText),
                    closeSoftKeyboard()
                );

            // Activo el botón de enviar para la acción editar
            onView(COMP_circle.FORM_CONTROL_sendData)
                .perform(click());
        }

        // Evalúo si se mostró el Toast sobre que la actualización fue exitosa
        onView(withText(activity.getResources().getString(R.string.dataBase_updated).replace("#####", text + updatedText)))
            .inRoot(new MIX_TEST_toast())
            .check(matches(isDisplayed()));

        // Valido si el botón de nuevo registro esta invisible y además reviso si el formulario esta limpio
        CONTROL_circleACTIONS.isClearForm();

        PANEL_circleACTIONS.changeFragment(activity,"MENU_TABLE");

        // Reviso si existe el registro ya creado
        onView(allOf(withText(text + updatedText), isDescendantOfA(withId(R.id.list_circles))))
            .check(matches(isDisplayed()));

        // Reviso si no existe el registro ya creado
        onView(allOf(withText(text), isDescendantOfA(withId(R.id.list_circles))))
            .check(doesNotExist());

        PANEL_circleACTIONS.changeFragment(activity,"MENU_FORM");

        // Pregunto si deseo restaurar los resultado a su estado original
        if (restart) {
            removeCircle(activity, text + updatedText, false);

            newCircle(activity, text);
        }
    }

    public static void removeCircle (Activity activity, String text, boolean testCancel) {
        // Cambio el fragmento a la tabla
        PANEL_circleACTIONS.changeFragment(activity,"MENU_TABLE");

        onView(allOf(withText(text), isDescendantOfA(withId(R.id.list_circles))))
                .check(matches(isDisplayed()));

        onView(allOf(withId(R.id.list_basic_A_button_B), isDescendantOfA(hasSibling(allOf(withText(text), isDescendantOfA(withId(R.id.list_circles)))))))
                .check(matches(isDisplayed()))
                .perform(click());

        if (testCancel) {
            onView(withText(R.string.modal_removeRegister_cancel))
                    .inRoot(isDialog())
                    .check(matches(isDisplayed()))
                    .perform(click());

            onView(allOf(withId(R.id.list_basic_A_button_B), isDescendantOfA(hasSibling(allOf(withText(text), isDescendantOfA(withId(R.id.list_circles)))))))
                    .check(matches(isDisplayed()))
                    .perform(click());
        }

        onView(withText(R.string.modal_removeRegister_remove))
                .inRoot(isDialog())
                .check(matches(isDisplayed()))
                .perform(click());

        onView(withText(activity.getResources().getString(R.string.dataBase_removed).replace("#####", text)))
                .inRoot(new MIX_TEST_toast())
                .check(matches(isDisplayed()));

        PANEL_circleACTIONS.changeFragment(activity,"MENU_FORM");
    }

}
