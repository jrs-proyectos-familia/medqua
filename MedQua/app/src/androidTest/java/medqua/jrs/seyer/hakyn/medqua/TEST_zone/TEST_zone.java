package medqua.jrs.seyer.hakyn.medqua.TEST_zone;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import medqua.jrs.seyer.hakyn.medqua.TEST_zone.tests.control.CONTROL_zoneCLEAN;
import medqua.jrs.seyer.hakyn.medqua.TEST_zone.tests.form.FORM_zoneACTIONS;
import medqua.jrs.seyer.hakyn.medqua.activities.ACTI_zone;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone;
import medqua.jrs.seyer.hakyn.medqua.bd.request.REQ_zone;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 20/03/19 a las 01:11 PM
 * @license Joaquin Reyes Sanchez
 */

@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TEST_zone {

	private String[] zones = new String[]{
		"Manzana A",
		"Manzana B",
		"Manzana C"
	};

	private ArrayList<DATA_zone> dataZones () {
		return REQ_zone.getAllZones(ruleZoneACTI.getActivity().getWindow().getContext());
	}

	@Rule
	public ActivityTestRule<ACTI_zone> ruleZoneACTI = new ActivityTestRule<>(ACTI_zone.class);

	@Test
	public void TEST_MAIN () {
		this.TEST_A_cleanZone();
		this.TEST_B_newZone();
		this.TEST_C_updateZone();
		this.TEST_D_updateImage();
		this.TEST_E_removeZone();
	}

	@Test
	public void TEST_A_cleanZone () {
		for (String zone : this.zones)
			CONTROL_zoneCLEAN.checkClean(ruleZoneACTI.getActivity(), zone);
	}

	@Test
	public void TEST_B_newZone () {
		for (String zone : this.zones)
			FORM_zoneACTIONS.newZone(ruleZoneACTI.getActivity(), zone);
	}

	@Test
	public void TEST_C_updateZone () {
		ArrayList<DATA_zone> dataZones = this.dataZones();

		for (DATA_zone zone : dataZones)
			FORM_zoneACTIONS.updateZone(ruleZoneACTI.getActivity(), zone.get_zone(), false);
	}

	@Test
	public void TEST_D_updateImage () {
		ArrayList<DATA_zone> dataZones = this.dataZones();

		for (DATA_zone zone : dataZones)
			FORM_zoneACTIONS.updateImage(ruleZoneACTI.getActivity(), zone.get_zone());
	}

	@Test
	public void TEST_E_removeZone () {
		ArrayList<DATA_zone> dataZones = this.dataZones();

		for(DATA_zone zone : dataZones)
			FORM_zoneACTIONS.removeZone(ruleZoneACTI.getActivity(), zone.get_zone(), false);
	}
}
