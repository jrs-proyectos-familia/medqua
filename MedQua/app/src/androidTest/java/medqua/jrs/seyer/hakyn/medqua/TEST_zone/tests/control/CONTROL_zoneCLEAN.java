package medqua.jrs.seyer.hakyn.medqua.TEST_zone.tests.control;

import android.app.Activity;
import medqua.jrs.seyer.hakyn.medqua.TEST_zone.COMP_zone;
import medqua.jrs.seyer.hakyn.medqua.TEST_zone.mixins.MIX_imageIV;
import medqua.jrs.seyer.hakyn.medqua.TEST_zone.mixins.MIX_zoneET;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withTagValue;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 20/03/19 a las 01:38 PM
 * @license Joaquin Reyes Sanchez
 */

public class CONTROL_zoneCLEAN extends COMP_zone {

	public static void checkCleanFields() {
		onView(COMP_zone.FORM_INPUT_zone)
			.check(matches(withText("")));

		// Selecciono el minicuadro visor de imagen y compruebo que su tagValue sea igual ""
		onView(COMP_zone.FORM_INPUT_image)
			.check(matches(withTagValue(is(""))));
	}

	public static void checkCleanFields(String textZone, String stateImage) {
		onView(COMP_zone.FORM_INPUT_zone)
			.check(matches(withText(textZone)));

		if (!stateImage.equals("UPDATE"))
			onView(COMP_zone.FORM_INPUT_image)
				.check(matches(withTagValue(is(""))));
		else
			onView(COMP_zone.FORM_INPUT_image)
				.check(matches(withTagValue(is(not("")))));
	}

	public static void checkCleanErrors() {
		onView(COMP_zone.FORM_ERROR_image)
			.check(matches(withText("")));

		onView(COMP_zone.FORM_ERROR_zone)
			.check(matches(withText("")));
	}

	public static void clearForm() {
		onView(COMP_zone.FORM_CONTROL_cleanForm)
			.perform(click());

		checkCleanFields();
		checkCleanErrors();
	}

	public static void clearForm (String textZone, String stateImage) {
		onView(COMP_zone.FORM_CONTROL_cleanForm)
			.perform(click());

		checkCleanFields(textZone, stateImage);
		checkCleanErrors();
	}

	public static void checkClean(Activity activity, String zoneText) {
		MIX_zoneET.writeZone(zoneText);
		MIX_imageIV.chargeImageFromGallery(activity, true);

		clearForm();
	}

}
