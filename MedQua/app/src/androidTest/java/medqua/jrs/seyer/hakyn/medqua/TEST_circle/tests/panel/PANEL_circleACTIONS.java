package medqua.jrs.seyer.hakyn.medqua.TEST_circle.tests.panel;

import android.app.Activity;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.TEST_circle.COMP_circle;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.mixins.MIX_circle_VARIABLES;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.data.DATA_circle_ACTI;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 19/03/19 a las 12:01 PM
 * @license Joaquin Reyes Sanchez
 */

public class PANEL_circleACTIONS extends COMP_circle {

    public static void changeFragment (Activity activity, String menu) {
        switch (menu) {
            case "MENU_FORM":
                DATA_circle_ACTI circleVARS = MIX_circle_VARIABLES.getVariablesFromMyActivity(activity.getWindow().getContext());

                switch (circleVARS.getTypeRegister()) {
                    case "TVS_NEW":
                        onView(PANEL_circleACTIONS.PANEL_newRegister)
                            .perform(click())
                            .check(matches(withText(R.string.circle_panel_newRegister)));
                        break;
                    case "TVS_UPDATE":
                        onView(PANEL_circleACTIONS.PANEL_newRegister)
                            .perform(click())
                            .check(matches(withText(R.string.circle_panel_editRegister)));
                        break;
                }
                break;
            case "MENU_TABLE":
                onView(PANEL_circleACTIONS.PANEL_list)
                    .perform(click())
                    .check(matches(withText(R.string.circle_panel_list_circles)));
                break;
        }
    }

}
