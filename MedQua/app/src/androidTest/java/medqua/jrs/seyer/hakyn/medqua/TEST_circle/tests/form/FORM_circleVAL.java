package medqua.jrs.seyer.hakyn.medqua.TEST_circle.tests.form;

import android.app.Activity;
import medqua.jrs.seyer.hakyn.medqua.mixins.MIX_TEST_validation;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.TEST_circle.COMP_circle;
import medqua.jrs.seyer.hakyn.medqua.mixins.MIX_TEST_toast;
import medqua.jrs.seyer.hakyn.medqua.TEST_circle.tests.control.CONTROL_circleACTIONS;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 19/03/19 a las 06:29 PM
 * @license Joaquin Reyes Sanchez
 */

public class FORM_circleVAL extends COMP_circle {

	public static void valEmptyCircle(Activity activity, String text) {
		// Validación Vacia
		String errorEmpty = activity.getResources().getString(R.string.validator_empty);
		MIX_TEST_validation.valInputError(null, "", COMP_circle.FORM_ERROR_circle, errorEmpty, COMP_circle.FORM_CONTROL_sendData);
		onView(withText(activity.getResources().getString(R.string.toast_form_missingDataForSend)))
			.inRoot(new MIX_TEST_toast())
			.check(matches(isDisplayed()));

		// Limpiar Formulario
		CONTROL_circleACTIONS.clearForm("");
	}

	public static void valMinCircle(Activity activity, String text) {
		// Validación Minimo
		String textMin = text;
		if (text.length() > FORM_circleACTIONS.RULE_circle.getValMin())
			textMin = text.charAt(0) + "" + text.charAt(1);
		else if (text.length() == FORM_circleACTIONS.RULE_circle.getValMin()) {
			textMin = "HS";
		}
		int missingCharacters = FORM_circleACTIONS.RULE_circle.getValMin() - textMin.length();
		String errorMin = activity.getResources().getString(R.string.validator_min).replace("#####", String.valueOf(missingCharacters));
		MIX_TEST_validation.valInputError(COMP_circle.FORM_INPUT_circle, textMin, COMP_circle.FORM_ERROR_circle, errorMin, COMP_circle.FORM_CONTROL_sendData);

		// Limpiar Formulario
		CONTROL_circleACTIONS.clearForm("");
	}

	public static void valMaxCircle(Activity activity, String text) {
		// Validación Máxima
		String textMax = text;
		if (text.length() < RULE_circle.getValMax()) {
			int missingText = RULE_circle.getValMax() - text.length();

			for (int i = 0; i < missingText + 1; i++)
				textMax += "X";
		} else if (text.length() == RULE_circle.getValMax())
			textMax += "X";

		int exceededCharacters = textMax.length() - RULE_circle.getValMax();
		String errorMax = activity.getResources().getString(R.string.validator_max).replace("#####", String.valueOf(exceededCharacters));
		MIX_TEST_validation.valInputError(COMP_circle.FORM_INPUT_circle, textMax, COMP_circle.FORM_ERROR_circle, errorMax, COMP_circle.FORM_CONTROL_sendData);

		// Limpiar Formulario
		CONTROL_circleACTIONS.clearForm("");
	}

	public static void valUniqueCircle(Activity activity, String text) {
		// Validación Unica
		String errorUnique = activity.getResources().getString(R.string.validator_unique).replace("#####", text);

		MIX_TEST_validation.valInputError(COMP_circle.FORM_INPUT_circle, text, COMP_circle.FORM_ERROR_circle, errorUnique, COMP_circle.FORM_CONTROL_sendData);

		CONTROL_circleACTIONS.clearForm("");
	}

	public static void valSuccessCircle(Activity activity, String text) {
		// Validación Exitosa
		MIX_TEST_validation.valInputError(COMP_circle.FORM_INPUT_circle, text, COMP_circle.FORM_ERROR_circle, "", COMP_circle.FORM_CONTROL_sendData);

		onView(withText(activity.getResources().getString(R.string.dataBase_created).replace("#####", text)))
			.inRoot(new MIX_TEST_toast())
			.check(matches(isDisplayed()));

		onView(COMP_circle.FORM_INPUT_circle)
			.check(matches(withText("")));
	}

}
