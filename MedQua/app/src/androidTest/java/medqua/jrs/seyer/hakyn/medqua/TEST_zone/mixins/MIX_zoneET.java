package medqua.jrs.seyer.hakyn.medqua.TEST_zone.mixins;

import medqua.jrs.seyer.hakyn.medqua.TEST_zone.COMP_zone;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 20/03/19 a las 02:07 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_zoneET extends COMP_zone {

    public static void writeZone (String text) {
        onView(COMP_zone.FORM_INPUT_zone)
            .perform(
                typeText(text),
                closeSoftKeyboard()
            )
            .check(matches(withText(text)));
    }

}
