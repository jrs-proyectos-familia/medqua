package medqua.jrs.seyer.hakyn.medqua.mixins;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.test.espresso.intent.Intents;
import org.hamcrest.Matcher;

import java.io.File;

import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static org.hamcrest.Matchers.allOf;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 21/03/19 a las 11:27 AM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_TEST_image {

    private static String imageTest = "android_image_A_test.png";

    public static String routeImageFromMemoryInternal (String category) {
        return "images/" + category + "/" + MIX_TEST_image.imageTest;
    }

    public static Uri getImageFromMemoryInternal () {
        // Es necesario guardar una imagen con el nombre de "android_image_A_test.png" en la dirección default de la memoria interna del dispositivo
        String filePath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filePath, MIX_TEST_image.imageTest);
        Uri imageUri = Uri.fromFile(file);

        return imageUri;
    }

    public static Matcher<Intent> testGalleryImage () {
        Intent resultData = new Intent();
        resultData.setData(MIX_TEST_image.getImageFromMemoryInternal());
        Instrumentation.ActivityResult result = new Instrumentation.ActivityResult(Activity.RESULT_OK, resultData);

        Matcher<Intent> expectedIntent = allOf(hasAction(Intent.ACTION_PICK),
                hasData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI));
        Intents.init();
        intending(expectedIntent).respondWith(result);

        return expectedIntent;
    }

}
