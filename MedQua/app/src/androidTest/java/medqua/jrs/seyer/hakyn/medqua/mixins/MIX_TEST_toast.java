package medqua.jrs.seyer.hakyn.medqua.mixins;

import android.os.IBinder;
import android.support.test.espresso.Root;
import android.view.WindowManager;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 19/03/19 a las 03:48 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_TEST_toast extends TypeSafeMatcher<Root> {


    @Override
    protected boolean matchesSafely(Root item) {
        int type = item.getWindowLayoutParams().get().type;

        if ((type == WindowManager.LayoutParams.TYPE_TOAST)) {
            IBinder windowToken = item.getDecorView().getWindowToken();
            IBinder appToken = item.getDecorView().getApplicationWindowToken();
            if (windowToken == appToken) return true;
        }
        return false;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("A Simple Toast");
    }
}
