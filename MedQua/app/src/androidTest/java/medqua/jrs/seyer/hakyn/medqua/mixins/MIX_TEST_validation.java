package medqua.jrs.seyer.hakyn.medqua.mixins;

import android.app.Activity;
import android.view.View;
import medqua.jrs.seyer.hakyn.medqua.R;
import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 21/03/19 a las 10:38 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_TEST_validation {

    /**
     *
     * @param inputView El input el cuál va a ingresar un valor a testear
     * @param dataText El texto el cuál va a ser asignado al input y posterior a ser evaluado en el testeo
     * @param errorView El error view el cual va a ser la validación deseada
     * @param errorMatch El texto que va a ser la comprobación del testeo
     * @param buttonSend El botón de envio para accionar el testeo de los errores
     */
    public static void valInputError (Matcher<View> inputView, String dataText, Matcher<View> errorView, String errorMatch, Matcher<View> buttonSend) {
        // Ingreso el valor al input tipo text
        if (inputView != null) {
            onView(inputView)
                .perform(
                    typeText(dataText),
                    closeSoftKeyboard()
                )
                .check(
                    matches(withText(dataText))
                );
        }

        if (buttonSend != null) {
            // Oprimo el botón de enviar
            onView(buttonSend)
                .perform(click());

            // Reviso si se mostró el texto del error correspondiente al test
            onView(errorView)
                .check(
                    matches(withText(errorMatch))
                );
        }
    }

    public static String getMessageErrorEmpty (Activity activity) {
        return activity.getResources().getString(R.string.validator_empty);
    }

    /**
     *
     * @param activity Activity de donde se esta haciendo el test
     * @param text Texto que se añadirá al mensaje de error
     * @return
     */
    public static String getMessageErrorMin (Activity activity, String text) {
        return activity.getResources().getString(R.string.validator_min).replace("#####", text);
    }

    /**
     *
     * @param activity Activity de donde se esta haciendo el test
     * @param text Texto que se añadirá al mensaje de error
     * @return
     */
    public static String getMessageErrorMax (Activity activity, String text) {
        return activity.getResources().getString(R.string.validator_max).replace("#####", text);
    }

    /**
     *
     * @param activity Activity de donde se esta haciendo el test
     * @param text Texto que se añadirá al mensaje de error como el titulo único existente
     * @return
     */
    public static String getMessageErrorUnique (Activity activity, String text) {
        return activity.getResources().getString(R.string.validator_unique).replace("#####", text);
    }

}
