package medqua.jrs.seyer.hakyn.medqua.TEST_zone.mixins;

import android.app.Activity;
import android.content.Intent;
import android.support.test.espresso.intent.Intents;
import medqua.jrs.seyer.hakyn.medqua.mixins.MIX_TEST_image;
import medqua.jrs.seyer.hakyn.medqua.mixins.MIX_TEST_toast;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.TEST_zone.COMP_zone;
import org.hamcrest.Matcher;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 20/03/19 a las 02:08 PM
 * @license Joaquin Reyes Sanchez
 */

public class MIX_imageIV extends COMP_zone {

	private static void selectOptionDialog(boolean gallery) {
		if (gallery) {
			onView(withId(R.id.modal_cameraOrGallery_gallery))
				.inRoot(isDialog())
				.check(matches(isDisplayed()))
				.perform(click());
		} else {
			onView(withId(R.id.modal_cameraOrGallery_camera))
				.inRoot(isDialog())
				.check(matches(isDisplayed()))
				.perform(click());
		}
	}

	public static void chargeImageFromGallery(Activity activity, boolean isNewRegister) {
		// Hago click en el visor de la imagen
		if (isNewRegister) {
			onView(COMP_zone.FORM_INPUT_image)
				.perform(click());
			// Evalúo si se muestra el Toast por no cargar ninguna imagen
			onView(withText(activity.getResources().getString(R.string.toast_image_previewNeed)))
				.inRoot(new MIX_TEST_toast())
				.check(matches(isDisplayed()));
		} else {
			// Compruebo el visualizador de la imagen
			onView(COMP_zone.FORM_INPUT_image)
				.perform(click());
			onView(withText(R.string.modal_exit))
				.inRoot(isDialog())
				.perform(click());
		}

		// Aquí simulo la carga de una imagen desde la galeria
		Matcher<Intent> expectedIntent = MIX_TEST_image.testGalleryImage();
		onView(COMP_zone.FORM_ACTION_image)
			.perform(click());
		selectOptionDialog(true);
		intended(expectedIntent);
		Intents.release();

		// Aqui verifico si la imagen ha sido cargada
		onView(withText(activity.getResources().getString(R.string.toast_image_save)))
			.inRoot(new MIX_TEST_toast())
			.check(matches(isDisplayed()));
		onView(COMP_zone.FORM_INPUT_image).check(matches(isDisplayed()));
		onView(COMP_zone.FORM_INPUT_image)
			.check(matches(withTagValue(is(not("")))));

		// Compruebo el visualizador de la imagen
		onView(COMP_zone.FORM_INPUT_image)
			.perform(click());
		onView(withText(R.string.modal_exit))
			.inRoot(isDialog())
			.perform(click());
	}

}
