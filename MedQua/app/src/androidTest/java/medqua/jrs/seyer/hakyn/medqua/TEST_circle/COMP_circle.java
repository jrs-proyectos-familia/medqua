package medqua.jrs.seyer.hakyn.medqua.TEST_circle;

import android.view.View;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.activities.circle.tvs.rules.RULE_circle_CIRCLE;
import org.hamcrest.Matcher;

import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 19/03/19 a las 11:42 AM
 * @license Joaquin Reyes Sanchez
 */

public class COMP_circle {

	// DEL FORMULARIO
	// Inputs
	protected static Matcher<View> FORM_INPUT_circle = withId(R.id.form_circle_input_circle);
	// Errors
	protected static Matcher<View> FORM_ERROR_circle = withId(R.id.form_circle_error_circle);

	// Controls
	protected static Matcher<View> FORM_CONTROL_newRegister = withId(R.id.form_control_newRegister);
	protected static Matcher<View> FORM_CONTROL_cleanForm = withId(R.id.form_control_cleanForm);
	protected static Matcher<View> FORM_CONTROL_sendData = withId(R.id.form_control_sendData);

	// PANEL
	protected static Matcher<View> PANEL_newRegister = withId(R.id.panel_newRegister);
	protected static Matcher<View> PANEL_list = withId(R.id.panel_list);

	// DE LAS REGLAS
	protected static RULE_circle_CIRCLE RULE_circle = new RULE_circle_CIRCLE();

}
