package medqua.jrs.seyer.hakyn.medqua.TEST_zone.tests.panel;

import android.app.Activity;
import medqua.jrs.seyer.hakyn.medqua.R;
import medqua.jrs.seyer.hakyn.medqua.TEST_zone.COMP_zone;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.data.DATA_zone_ACTI;
import medqua.jrs.seyer.hakyn.medqua.activities.zone.mixins.MIX_zone_VARIABLES;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Archivo MedQua
 *
 * @author Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
 * Creado el día 20/03/19 a las 01:21 PM
 * @license Joaquin Reyes Sanchez
 */

public class PANEL_zoneACTIONS extends COMP_zone {

    public static void changeFragment (Activity activity, String menu) {
        switch (menu) {
            case "MENU_FORM":
                DATA_zone_ACTI zoneACTI = MIX_zone_VARIABLES.getVariablesFromMyActivity(activity.getWindow().getContext());

                switch (zoneACTI.getTypeRegister()) {
                    case "TVS_NEW":
                        onView(COMP_zone.PANEL_newRegister)
                            .perform(click())
                            .check(matches(withText(R.string.zone_panel_newRegister)));
                        break;
                    case "TVS_UPDATE_ZONE":
                        onView(COMP_zone.PANEL_newRegister)
                            .perform(click())
                            .check(matches(withText(R.string.zone_panel_editRegister)));
                        break;
                    case "TVS_UPDATE_IMAGE":
                        onView(COMP_zone.PANEL_newRegister)
                            .perform(click())
                            .check(matches(withText(R.string.zone_panel_editRegister)));
                        break;
                }
                break;
            case "MENU_TABLE":
                onView(COMP_zone.PANEL_list)
                        .perform(click())
                        .check(matches(withText(R.string.zone_panel_list_zones)));
                break;
        }
    }

}
